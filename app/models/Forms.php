<?php

class Forms extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'evoluation_form';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	public function topic()
	{
		return $this->hasMany('Topic','evoluation_form_form_id','id');
	}
	public function resultEvo()
	{
		return $this->hasMany('ResultEvo','form_id','id');
	}

}
