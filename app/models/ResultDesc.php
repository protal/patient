<?php

class ResultDesc extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'result_desc';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	public static  function findForm($id)
	{
		return ResultDesc::where('form_id',$id)->orderBy('score_start')->get();
	}

}
