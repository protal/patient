<?php

class Person extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'person';

	protected $primaryKey = 'pid';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
    protected $hidden = array();

    public function house()
    {
        return $this->belongsTo('house','pidvola','pid');
    }
}
