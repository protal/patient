<?php

class Patienthistory extends Eloquent {

  protected $table = 'patient_history';

  public function patient()
	{
		return $this->belongsTo('patient','patient_id','id');
	}
  public function datesubmit()
  {
    $get_array = explode('-', $this->attributes['datesubmit']);
    $day = $get_array[2];
    $month = $get_array[1];
    $year = $get_array[0]+543;
    return $day.'-'.$month.'-'.$year;
  }
}
