<?php

class Village extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'villages';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
  protected $hidden = array();
  
  public function getVillageCode()
  {
    return $this->provcode.$this->distcode.$this->subdistcode.sprintf("%02d", $this->village);
  }

}
