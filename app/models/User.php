<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Carbon\Carbon;


class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	use SoftDeletingTrait;
	protected $table = 'users';
	protected $guarded = array('id', 'username',);
	protected $dates = ['deleted_at'];


	public function isAdmin()
	{

		if($this->type == 0)
			return true ;
		return false;
	}
	public function types()
    {
        return $this->belongsTo('Type','type','id');
    }
    public function lastTimeLogin()
    {
        Carbon::setLocale('th');
        $log = Loginlogs::where('user_id',$this->attributes['id'])->orderBy('created_at','DESC')->first();
        if(isset($log->created_at))
        {
            $time = new Carbon($log->created_at);
            return $time->diffForHumans();
        }
        else
        {
            return "ไม่พบวันที่เข้าสู่ระบบล่าสุด";
        }

    }


	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

}
