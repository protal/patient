<?php

class Patient extends Eloquent {

    protected $table = 'patient';

    public function Result()
    {
        return $this->hasMany('ResultEvo','patient_id','id');
    }
    public function patienthistory()
        {
            return $this->hasMany('patienthistory','patient_id','id');
        }
    public function age()
    {
        return (Carbon::now()->year) - Carbon::createFromFormat('Y-m-d', $this->attributes['Birthday'])->year;
    }
    public function Birthday()
    {
        $get_array = explode('-', $this->attributes['Birthday']);
        $day = $get_array[2];
        $month = $get_array[1];
        $year = $get_array[0]+543;
        return $day.'-'.$month.'-'.$year;
    }
    public function volunteer()
    {
        $villcode = $this->attributes['Province'].$this->attributes['District'].$this->attributes['Subdistrict'].sprintf("%02d",$this->attributes['Village']);
        $house = House::where('hno',$this->attributes['HomeNo'])->where('villcode',$villcode)->first();
        if($house)
        {
            $person = Person::where('pid',$house->pidvola)->first();
            return $person;
        }
        else {
            return null;
        }
    }
}
