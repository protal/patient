<?php

class RecordTime extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'record_time';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	public function result($patientID,$formID)
	{
		return ResultEvo::where('patient_id',$patientID)->where('form_id', $formID)->where('years',$this->attributes['record_years'])->where('times',$this->attributes['record_times'])->first();
	}
	public function isRecordTime($years,$times)
	{
		$r = RecordTime::where('record_years',$years)->where('record_times',$times)->first();
		return isset($r);
	}
}
