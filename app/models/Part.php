<?php

class Part extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'evoluation_part';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();
	
	public function forms()
	{
		return $this->hasMany('Forms','part_id','id');
	}

}
