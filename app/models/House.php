<?php

class House extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'House';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
    protected $hidden = array();

    public function endcodeVillcode($vilcode)
    {
        $pcode = Config::get('app.province');
        $dcode = Config::get('app.district');
        $scode = Config::get('app.subdist');
        $vcode =  sprintf("%02d",$vilcode);

        return $pcode.$dcode.$scode.$vcode;
    }

}
