<?php

class Topic extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'evoluation_topic';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	public static  function findForm($id)
	{
		return Topic::where('evoluation_form_form_id',$id)->get();
	}
	public function scores()
  {
      return $this->hasMany('Score','evoluation_topic_topic_id','id');
  }

}
