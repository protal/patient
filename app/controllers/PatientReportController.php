<?php

class PatientReportController extends \BaseController {

  public function index($id)
  {
    $patients = Patient::find($id);
    $parts = Part::all() ;
    return View::make('patientReport')->with('parts',$parts)->with('patients',$patients);
  }

  
}
