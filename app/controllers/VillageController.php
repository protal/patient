<?php
  class VillageController extends BaseController {

  public function index(){
    $page = 10;
    $village = Village::orderBy('village','asc');
    if(!empty(Input::get('q')))
    {
      $q = Input::get('q');
      $village = $village->where('village','like', '%'.$q.'%')
                  ->orwhere('subdistcode', 'like', '%'.$q.'%');

    }
    else{
      $q = null;
    }
    $village = $village->paginate($page);
    return View::make('admin.village')
                 ->with('villages',$village)
                 ->with('q',$q);
  }

  public function addVillageView()
  {
    return View::make('admin.villageAdd');
  }

  public function addVillage()
  {
    $io = Input::all();
    if (Village::where('village', '=', $io['village'])->count() > 0) {
       // Village found
       return Redirect::to('admin/village/add')->with('error', 'หมู่ที่ : '.$io['village'].' ไม่สามารถเพิ่มได้ เพราะอยู่ในระบบเเล้ว')->withInput();
    }
    $village = new Village;
    $village->provcode = '80';
    $village->distcode = '08';
    $village->subdistcode = '09';

    $village->village = $io["village"];
    $village->centercoord = $io["centercoord"];
    $village->edgecoord = $io["edgecoord"];
    $village->color = $io["color"];
    $village->save();
    return Redirect::to('admin/village')->with('message',' บันทึกหมู่ที่ '.$io['village']. ' สำเร็จ');
  }

  public function delvillage($id)
  {
    $village = Village::find($id);

    if (isset($village)) {
      $village_code = $village->getVillageCode();
      $house = House::where('villcode',$village_code)->count();
      if($house > 0 ){
        return Redirect::to('admin/village')->with('error', 'ไม่สามารถลบได้เนื่องจากมีบ้านเลขที่อยู่ในระบบ');
      }
      try {
        $village->delete();
      } catch (\Throwable $th) {
        
      }
    }
    return Redirect::to('admin/village')->with('message', 'ลบตำบลที่บันทึกและหมู่บ้านที่บันทึกสำเร็จ');
  }

  public function editvillageView($id)
  {
    $io = Input::all();
    $village = Village::find($id);
    if (empty($village))
      return Redirect::to('admin/village');

    //colorselection
    $village->blue = ($village->color=="blue")? "SELECTED":"" ;
    $village->orange = ($village->color=="orange")? "SELECTED":"" ;
    $village->purple = ($village->color=="purple")? "SELECTED":"" ;
    $village->green = ($village->color=="green")? "SELECTED":"" ;
    $village->pink = ($village->color=="pink")? "SELECTED":"" ;
    $village->yellow = ($village->color=="yellow")? "SELECTED":"" ;

    return View::make('admin.villageEdit')->with('village',$village);
  }

  public function editvillage($id)
  {
    $io = Input::all();
    $village = Village::find($id);

    if (empty($village))
      return Redirect::to('admin/village');
    $village->subdistcode = '09';
    $village->village = $io["village"];
    $village->centercoord = $io["centercoord"];
    $village->edgecoord = $io["edgecoord"];
    $village->color = $io["color"];
    $village->save();
    return Redirect::to('admin/village')->with('message', ' แก้ไขข้อมูลหมู่ที่ '.$io['village']. ' สำเร็จ');
    }
  }

 ?>
