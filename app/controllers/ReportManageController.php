<?php

class ReportManageController extends \BaseController {


  public function index()
  {
    $parts = Part::all() ;
    return View::make('admin.report')->with('parts',$parts);
  }

  public function partAddView()
  {
    return View::make('admin.reportPartAdd');
  }

  public function partAdd()
  {
    $io = Input::all();

    if (Part::where('name', '=', $io['name'])->count() > 0) {
      return Redirect::to('admin/report')->with('error', 'ไม่สามารถเพิ่มได้เนื่องจากมีส่วนการประเมิณ '. $io['name'].' อยู่ในระบบแล้ว');
    }

    $part = new Part ;
    $part->name = $io['name'];
    $part->save();
    return Redirect::to('admin/report')->with('message', 'เพิ่ม '.$io['name'].' ส่วนสำเร็จ');
  }


  public function partDel($id)
  {
    //ตรวจ path ว่ายังมีค่ามั้ย
    $count_form = Forms::where('part_id',$id)->count();
    if($count_form > 0)
      return Redirect::to('admin/report')->with('error', 'ไม่สามารถลบได้เนื่องจากมีหัวข้อบันทึกทั้งหมด '.$count_form.' หัวข้อข้างใน');


    $part = Part::find($id);
    $partName = $part->name;
    $part->delete();
    return Redirect::to('admin/report')->with('message', 'ลบหัวข้อ '.$partName.' สำเร็จ');
  }

  public function partEditView($id)
  {
    $part = Part::find($id);
    return View::make('admin.reportPartEdit')->with('part',$part);
  }

  public function partEdit($id)
  {
    $io = Input::all();
    $part = Part::find($id);
    $part->name = $io['name'];
    $part->save();
    return Redirect::to('admin/report')->with('message', 'แก้ไข' .$io['name']. 'สำเร็จ');
  }

  public function formAddView()
  {
    return View::make('admin.reportFormAdd');
  }
  public function formAdd($id)
  {
    $io = Input::all();
    if (Forms::where('name', '=', $io['name'])->count() > 0) {
      return Redirect::to('admin/report')->with('error', 'ไม่สามารถเพิ่มได้เนื่องจากมีหัวข้อการบันทึก '. $io['name'].' อยู่ในระบบแล้ว');
    }

    $form = new Forms;
    $form->name = $io['name'];
    $form->part_id = $id;
    $form->save();
    return Redirect::to('admin/report')->with('message', 'เพิ่มหัวข้อบันทึก '.$io['name'].' ส่วนสำเร็จ');

  }
  public function formView($id)
  {
    $form = Forms::find($id);
    $topics = Topic::findForm($id);
    $resultDescs = ResultDesc::findForm($id);
    return View::make('admin.reportForm')->with('form',$form)->with('topics', $topics)->with('resultDescs', $resultDescs);
  }

  public function formEditView($id)
  {
    $form = Forms::find($id);

    return View::make('admin.reportFormEdit')->with('form',$form);
  }
  public function formEdit($id)
  {
    $io = Input::all();
    $form = Forms::find($id);
    $form->name = $io['name'];
    $form->save();
    return Redirect::to('/admin/report/form/'.$id)->with('message', 'แก้ไขสำเร็จ');
  }

  public function formDel($id)
  {
    $form = Forms::find($id);
    $formName = $form->name;

        try {
            $form->delete();
        } catch (Exception $e) {
            return Redirect::to('admin/report')->with('error', 'ไม่สามารถลบ '.$formName.' ได้เนื่องจากมีข้อมูลด้านในหรือมีผู้ป่วยที่บันทึกผลการประเมินนี้เเล้ว');
        }

        return Redirect::to('admin/report')->with('message', 'ลบ '.$formName.' สำเร็จ');
  }


  public function topicAddView($id)
  {
    return View::make('admin.reportTopicAdd');
  }

  public function topicAdd($id)
  {
    $io = Input::all();
    $topic = new Topic;
    $topic->name = $io['name'];
    $topic->evoluation_form_form_id  = $id;
    $topic->save();

    for ($i=0; $i < count($io['scorename']); $i++) {
      $score = new Score ;
      $score->name = $io['scorename'][$i];
      $score->score = $io['score'][$i];
      $score->evoluation_topic_topic_id = $topic->id;
      $score->save();
    }
    return Redirect::to('/admin/report/form/'.$id)->with('message', 'เพิ่ม '. $io['name'].' สำเร็จ');
  }

  public function topicEditView($id)
  {
    $topic = Topic::find($id);

    return View::make('admin.reportTopicEdit')->with('topic',$topic);
  }
  public function topicEdit($id)
  {
    $io = Input::all();
    $topic = Topic::find($id);
    $topic->name = $io['name'];
    $topic->save();

    if(isset($io['scorename']))
    {
      for ($i=0; $i < count($io['scorename']); $i++) {
        $scoreid = $io['scoreid'][$i];
        if($scoreid == -1)
          $score = new Score ;
        else
          $score = Score::find($scoreid) ;
        $score->name = $io['scorename'][$i];
        $score->score = $io['score'][$i];
        $score->evoluation_topic_topic_id = $topic->id;
        $score->save();
      }
    }


    if(isset($io['del']))
    {
      for ($i=0; $i < count($io['del']) ; $i++) {
        $scoreid = $io['del'][$i];
        $score = Score::find($scoreid) ;
        $score->delete();
      }
    }


    return Redirect::to('/admin/report/form/'.$topic->evoluation_form_form_id)->with('message', 'แก้ไข '. $io['name'].' สำเร็จ');
  }
  public function topicDel($id)
  {
    $topic = Topic::find($id);
    $topicName = $topic->name;
    $formID = $topic->evoluation_form_form_id;
    $topic->delete();
    return Redirect::to('admin/report/form/'.$formID)->with('message', 'ลบ '.$topicName.' สำเร็จ');
  }




  public function resultAddView($id)
  {
    return View::make('admin.reportResultAdd');
  }
  public function resultEditView($id)
  {
    $result = ResultDesc::find($id);
    $result->rowcolor = $this->stringtocolor((isset($result->rowcolor)?$result->rowcolor:1));
    $result->pincolor = isset($result->pincolor)?$result->pincolor:1;
    return View::make('admin.reportResultEdit')->with('result',$result);
  }
  private function stringtocolor($value)
  {
    if($value == "rgba(255, 212, 0, 0.24)")
      return 2;
    else if($value == "rgba(255, 0, 0, 0.24)")
      return 3;
    else if($value == "rgba(255, 71, 0, 0.24)")
      return 4;
    else if($value == "rgba(255, 129, 0, 0.24)")
      return 5;
    else
      return 1;
  }
  private function colortostring($value)
  {
    if($value == 2)
      return "rgba(255, 212, 0, 0.24)";
    else if($value == 3)
      return "rgba(255, 0, 0, 0.24)";
    else if($value == 4)
      return "rgba(255, 71, 0, 0.24)";
    else if($value == 5)
      return "rgba(255, 129, 0, 0.24)";
    else
      return "rgba(0, 255, 20, 0.24)";
  }
  public function isNotInRank($id,$formID,$start,$end)
  {
    $resultDescs  = ResultDesc::where('form_id',$formID)->get();
    foreach ($resultDescs as $rDesc) {
        if($rDesc->id == $id)
            continue;
	    if($rDesc->score_end==0)
	    {
            if($start >= $rDesc->score_start || $end >= $rDesc->score_start)
            {

                return false;

            }
        }
        else{
	        if($start >= $rDesc->score_start && $start <= $rDesc->score_end)
            {

                return false;

            }
            if($end !=  0 && $end >= $rDesc->score_start && $end <= $rDesc->score_end)
	        {
                return false;

            }
        }
	}
    return true;
  }

  public function resultAdd($id)
  {
    $io = Input::all();

    $rowcolor = $this->colortostring((isset($io['rowcolor'])?$io['rowcolor']:1));
    $pincolor = ((isset($io['pincolor'])&&($io['pincolor']==3||$io['pincolor']==2))?$io['pincolor']:1);

    $result = new ResultDesc;

    if(!$this->isNotInRank($id,$result->form_id,$io['score_start'],$io['score_end'])){
        return Redirect::back()->with('error', 'ไม่สามารถเพิ่มได้เนื่องจากเลยช่วงที่กำหนด')->withInput();
    }

    $result->name = $io['name'];
    if(isset($io['desc']))
      $result->desc = $io['desc'];
    $result->score_start = $io['score_start'];
    $result->score_end = $io['score_end'];
    $result->form_id  = $id;
    $result->rowcolor = $rowcolor;
    $result->pincolor = $pincolor;
    $result->save();

    return Redirect::to('/admin/report/form/'.$id)->with('message', 'เพิ่ม '. $io['name'].' สำเร็จ');
  }
  public function resultEdit($id)
  {
    $io = Input::all();

    $rowcolor = $this->colortostring((isset($io['rowcolor'])?$io['rowcolor']:1));
    $pincolor = ((isset($io['pincolor'])&&($io['pincolor']==3||$io['pincolor']==2))?$io['pincolor']:1);

    $result = ResultDesc::find($id);

    if(!$this->isNotInRank($id,$result->form_id,$io['score_start'],$io['score_end'])){
        return Redirect::back()->with('error', 'ไม่สามารถเพิ่มได้เนื่องจากเลยช่วงที่กำหนด')->withInput();
    }


    $result->name = $io['name'];
    if(isset($io['desc']))
      $result->desc = $io['desc'];
    $result->score_start = $io['score_start'];
    $result->score_end = $io['score_end'];
    $result->rowcolor = $rowcolor;
    $result->pincolor = $pincolor;
    $form_id = $result->form_id ;
    $result->save();

    return Redirect::to('/admin/report/form/'.$form_id)->with('message', 'เพิ่ม '. $io['name'].' สำเร็จ');
  }

  public function resultDel($id)
  {
    $result =ResultDesc::find($id);
    $topicName = $result->name;
    $formID = $result->form_id;

    try {
        $result->delete();
    } catch (Exception $e) {
        return Redirect::to('admin/report/form/'.$formID)->with('error', 'ไม่สามารถลบ '.$topicName.' ได้เนื่องจากมีผลการประเมิน');
    }
    return Redirect::to('admin/report/form/'.$formID)->with('message', 'ลบ '.$topicName.' สำเร็จ');
  }

}
