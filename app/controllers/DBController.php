<?php

class DBController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$temp = DB::select("SELECT * FROM `temp_profile` ");
		foreach ($temp as $t) {
			$patient = Patient::find($t->id);

			if(isset($patient))
			{
				continue;
			}

			$patient = new Patient;
			$Patienthistory = new Patienthistory;
			$Patienthistory->patient_id = $t->id ;
			$patient->id = $t->id;
			$patient->prefix = $t->prefix;
			if($t->prefix == "นาง" || $t->prefix == "นางสาว")
			{
				$patient->gender = 0;
			}
			elseif($t->prefix == "นาย")
			{
				$patient->gender = 1;
			}
			$patient->Firstname = $t->Firstname;
			if(!isset($patient->Firstname))
				continue;
			$patient->Sirname = $t->Sirname;
			$patient->CitizenID = $t->CitizenID;
			if(!isset($t->Birthday))
				continue;

			$get_array = explode('/', $t->Birthday);
			$day = $get_array[0];
			$month = $get_array[1];
			$year = $get_array[2];
			$time_Stamp = mktime(0,0,0,$month,$day,$year);

			$patient->Birthday = $year.'-'.$month.'-'.$day ;
			$Patienthistory->Weight = $t->Weight;
			$Patienthistory->Height = $t->Height;
			$patient->HomeNo = $t->HomeNo;
			$patient->Village = $t->Village;
			$patient->Alley = $t->Alley;
			$patient->Road = $t->Road;
			$patient->Subdistrict = "09";
			$patient->District = "08";
			$patient->Province = "80";
			$patient->PostalCode = $t->PostalCode;
			$patient->Telephone = $t->Telephone;
			$patient->Job = $t->Job;
			$Patienthistory->Pastillness = $t->Pastillness;
			$Patienthistory->Historysurgery = $t->Historysurgery;
			$Patienthistory->Congenital = $t->Congenital;
			$Patienthistory->Drugidentification = $t->Drugidentification;
			$Patienthistory->Allergic = $t->Allergic;
			$Patienthistory->Cigarette = $t->Cigarette;
			$Patienthistory->Drink = $t->Drink;

			$Patienthistory->Time = 1 ;

			$Patienthistory->Datesubmit  = '2015-01-01';
			if(isset($t->datewirte))
			{
				$get_array = explode('/', $t->datewirte);
				$day = $get_array[0];
				$month = $get_array[1];
				$year = $get_array[2]-543;
				$time_Stamp = mktime(0,0,0,$month,$day,$year);
				$Patienthistory->Datesubmit = $year.'-'.$month.'-'.$day ;
			}



			$patient->save();
			$Patienthistory->save();
		}

	}
	public function insert7()
	{
	  set_time_limit(6000); //60 seconds = 1 minute
	  $f = file_get_contents('http://localhost/joinindata/read.txt');
	  $f_n = explode("\n",$f);
	  foreach ($f_n as $v) {
	    $field = explode(",",$v);
	    //date
			  $d = 1 ;
			  $m = 1 ;
			  $y = 2016;

	    $start = array(15,18,20,24,27,31,34,37,39,42,45);
	    $sum = 0 ;
	    for ($i=2; $i < 12 ; $i++) {

	      if(isset($field[$i]) && $field[$i]!= '')
	      {
	        $sum += $field[$i] ;
	        echo $field[$i]." ";
	        $evo_score = Score::where('evoluation_topic_topic_id','=',10+($i-2))->where('score','=',$field[$i])->first();
	        if(!isset($evo_score))
	        {
	          $field[$i] = '';
	        }
	        else {
	          $field[$i] = $evo_score->id;
	        }

	      }
	    }

	    echo "<br>";
	    $checked = true;
	    for ($i=2; $i < 12 ; $i++) {
	      if(isset($field[$i]) && $field[$i] !='')
	      {

	      }
	      else {
	        $checked = false;
	      }
	    }
	    if($checked)
	    {
	      $patient = Patient::find($field[0]);
	      if(!isset($patient))
	        continue;
	      $result = new ResultEvo;
	      $result->date = DateTime::createFromFormat('d/m/Y', $d.'/'.$m.'/'.$y)->format('Y-m-d H:i:s') ;
	      $result->patient_id = $patient->id;
	      $result->form_id = 7;
	      $result->save();

	      //unset($io['date']);
	      // $result->resultDetail()->delete();
	      $score = 0 ;
	      for ($i=2; $i < 12 ; $i++) {
	        // $sum  += $value ;
	        $resultDetail = new ResultDetail;
	        $resultDetail->evolu_id = $result->id;
	        $resultDetail->score_id = $field[$i];
	        $resultDetail->save();
	      }

	      $resultDescs_now = NULL;
	      $resultDescs  = ResultDesc::where('form_id',7)->get();

	      foreach ($resultDescs as $rDesc) {
	        if($rDesc->score_end==0)
	        {
	          if($sum >= $rDesc->score_start)
	            $resultDescs_now = $rDesc;
	        }
	        else{
	          if($sum >= $rDesc->score_start && $sum <= $rDesc->score_end)
	            $resultDescs_now = $rDesc;
	        }

	      }

	      if(isset($resultDescs_now))
	      {
	        $result->result_id = $resultDescs_now->id;
	        $result->result_name = $resultDescs_now->name;
	      }

	      $result->score = $sum;
	      $result->save();
	    }

	  }
	}
	public function insert()
	{
		set_time_limit(6000); //60 seconds = 1 minute
		$f = file_get_contents('http://localhost/joinindata/read.txt');
		$f_n = explode("\n",$f);
		foreach ($f_n as $v) {
		  $field = explode(",",$v);
		  //date
		  $d = 1 ;
		  $m = 1 ;
		  $y = 2016;

		  $start = array(49,54,59,64,69,74,79,84,89,94,99,104,);
		  $sum = 0 ;
		  for ($i=1; $i <= 12 ; $i++) {

		    if(isset($field[$i]) && $field[$i]!= '')
		    {
		      $sum += $field[$i] ;
		      echo $field[$i]." ";
					$evo_score = Score::where('evoluation_topic_topic_id','=',21+($i))
															->where('score','=',$field[$i])
															->first();
				  if(!isset($evo_score))
					{
						$field[$i] = '';
					}
					else {
						$field[$i] = $evo_score->id;
					}

		    }
		  }

		  echo "<br>";
			$checked = true;
			for ($i=1; $i <= 12 ; $i++) {
			  if(isset($field[$i]) && $field[$i] !='')
			  {

				}
				else {
					$checked = false;
				}
			}
			if($checked)
			{
				$patient = Patient::find($field[0]);
				if(!isset($patient))
					continue;
				$result = new ResultEvo;
				$result->date = DateTime::createFromFormat('d/m/Y', $d.'/'.$m.'/'.$y)->format('Y-m-d H:i:s') ;
				$result->patient_id = $patient->id;
				$result->form_id = 9;
				$result->save();

				//unset($io['date']);
				// $result->resultDetail()->delete();
				$score = 0 ;
				for ($i=1; $i <= 12 ; $i++) {
					// $sum  += $value ;
					$resultDetail = new ResultDetail;
					$resultDetail->evolu_id = $result->id;
					$resultDetail->score_id = $field[$i];
					$resultDetail->save();
				}

				$resultDescs_now = NULL;
				$resultDescs  = ResultDesc::where('form_id',9)->get();

				foreach ($resultDescs as $rDesc) {
					if($rDesc->score_end==0)
					{
						if($sum >= $rDesc->score_start)
							$resultDescs_now = $rDesc;
					}
					else{
						if($sum >= $rDesc->score_start && $sum <= $rDesc->score_end)
							$resultDescs_now = $rDesc;
					}

				}

				if(isset($resultDescs_now))
				{
					$result->result_id = $resultDescs_now->id;
					$result->result_name = $resultDescs_now->name;
				}

				$result->score = $sum;
				$result->save();
			}

		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
