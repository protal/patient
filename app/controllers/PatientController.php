<?php
class PatientController extends BaseController {

  public function index()
  {
    $page = 10;
    $patients = Patient::orderBy('Firstname');
    if(!empty(Input::get('q')))
    {
        $q = Input::get('q');

        if(!empty(Input::get('type')) && Input::get('type') == "volunteer")
        {
            $hcode = [];
            $persons =  DB::table('person')
                                ->where('fname','like', '%'.$q.'%')
                                ->orwhere('lname', 'like', '%'.$q.'%')
                                ->join('house','person.pid','=','house.pidvola')
                                ->get();
            $patientID = [];
            foreach($persons as $p)
            {
                $procode = $p->villcode[0].$p->villcode[1];
                $discode = $p->villcode[2].$p->villcode[3];
                $subcode = $p->villcode[4].$p->villcode[5];
                $vilcode = intval($p->villcode[6].$p->villcode[7]);
                $patient = Patient::where('Province',$procode)
                                    ->where('District',$discode)
                                    ->where('Subdistrict',$subcode)
                                    ->where('Village',$vilcode)
                                    ->where('HomeNo',$p->hno)
                                    ->first();
                $patientID[] = $patient['id'];
            }

            $patients = $patients->whereIn('id',$patientID);
        }

        else
        {
            $patients = $patients->where('Firstname','like', '%'.$q.'%')->orwhere('Sirname', 'like', '%'.$q.'%');
        }
    }
    else{
      $q = null;
    }
    $patients = $patients->paginate($page);
    return View::make('patient')
                 ->with('patients',$patients)
                 ->with('q',$q)
                 ->with('type',Input::get('type'));
  }

  public function addpatientView()
  {
      $vill = Village::orderBy('village')->get();
      return View::make('patientAdd', [ 'villcode' => $vill ] );
  }

  public function addpatient()
  {
    $io = Input::all();

    $get_array = explode('-', $io["birthday"]);
    $day = $get_array[0];
    $month = $get_array[1];
    $year = $get_array[2]-543;

    // dd($io);
    $Patient = new Patient;
    $Patienthistory = new Patienthistory;

    $Patient->prefix = $io["prefix"];
    $Patient->Firstname = $io["firstname"];
    $Patient->Sirname = $io["lastname"];
    $Patient->CitizenID = str_replace("-","",$io["idcard"]);
    $Patient->gender = ($io["prefix"]=="นาย")?1:0;
    $Patient->Birthday = $year.'-'.$month.'-'.$day ;

    $Patienthistory->Weight = $io["weight"];
    $Patienthistory->Height = $io["height"];

    $Patient->BloodType = $io["bloodtype"];
    $Patient->HomeNo = $io["homenum"];


    if(isset($io['moo']))
      $Patient->Village = $io["moo"];

    if(isset($io['soi']))
      $Patient->Alley = $io["soi"];

    if(isset($io['road']))
      $Patient->Road = $io["road"];


    $Patient->Subdistrict = $io["place"];
    $Patient->District = Config::get('app.district');
    $Patient->Province = Config::get('app.province');
    $Patient->PostalCode = $io["postnum"];

    if(isset($io['phone']))
      $Patient->Telephone = $io["phone"];
    if(isset($io['job']))
      $Patient->Job = $io["job"];

    if(isset($io['Sickhistory']))
      $Patienthistory->Pastillness = $io["Sickhistory"];
    if(isset($io['Surgeryhistory']))
      $Patienthistory->Historysurgery = $io["Surgeryhistory"];
    if(isset($io['congenital']))
      $Patienthistory->Congenital = $io["congenital"];
    if(isset($io['Drug']))
      $Patienthistory->Drugidentification = $io["Drug"];
    if(isset($io['Allergy']))
      $Patienthistory->Allergic = $io["Allergy"];


    $Patienthistory->Cigarette = $io["cigarette"];
    if(isset($io['yearUseto']))
      $Patienthistory->CigaretteTime = $io["yearUseto"];

    $Patienthistory->Drink = $io["alcohol"];
    if(isset($io['alcoholuseto']))
      $Patienthistory->Drinktime = $io["alcoholuseto"];

    $get_array = explode('-', $io["submitday"]);
    $days = $get_array[0];
    $months = $get_array[1];
    $years = $get_array[2]-543;
    $Patienthistory->datesubmit = $years.'-'.$months.'-'.$days ;
    // $Patienthistory->datesubmit = "2017-11-15" ;

    $Patient->save();
    $Patienthistory->patient_id = $Patient->id ;
    $Patienthistory->Time = 1;
    $Patienthistory->save();
    return Redirect::to('patient/'.$Patient->id.'/report')->with('message', 'เพิ่ม ผู้ป่วย : '.$io['firstname'].' สำเร็จ');
  }

  public function editpatientView($id)
  {
    $io = Input::all();
    $patients = Patient::find($id);
    if (empty($patients))
      return Redirect::to('patient');
    $patientsHistory = Patienthistory::where('patient_id',$id)
                                      ->orderBy('Time','desc')
                                      ->first();
    $maxTime = $patientsHistory->Time;
    $patientsHistory->Time++;
    if(isset($io["time"]))
    {
      $t_history = Patienthistory::where('Time',$io["time"])->where('patient_id',$id)->first();
      if(empty($t_history))
        return Redirect::to('patient/'.$patients->id.'/edit')->with('error', 'ไม่พบข้อมูลครั้งที่ '.$io["time"]);
      $patientsHistory = $t_history;
    }
    //prefixselection
    $patients->preMr = ($patients->prefix=="นาย")? "SELECTED":"" ;
    $patients->preMrs = ($patients->prefix=="นาง")? "SELECTED":"" ;
    $patients->preMiss = ($patients->prefix=="นางสาว")? "SELECTED":"" ;

    //genderselection
    $patients->genM = ($patients->gender=="1")? "SELECTED":"" ;
    $patients->genW = ($patients->gender=="0")? "SELECTED":"" ;

    //bloodtyoeselection
    $patients->btA = ($patients->BloodType=="A")? "SELECTED":"" ;
    $patients->btB = ($patients->BloodType=="B")? "SELECTED":"" ;
    $patients->btAB = ($patients->BloodType=="AB")? "SELECTED":"" ;
    $patients->btO = ($patients->BloodType=="O")? "SELECTED":"" ;

    //province
      $patients->pv22 = ($patients->Province=="นครศรีธรรมราช")? "SELECTED":"" ;


      //cigarette
      $patientsHistory->cig1 = ($patientsHistory->Cigarette==1)? "checked":"" ;
      $patientsHistory->cig2 = ($patientsHistory->Cigarette==2)? "checked":"" ;
      $patientsHistory->cig3 = ($patientsHistory->Cigarette==3)? "checked":"" ;
      $patientsHistory->cig4 = ($patientsHistory->Cigarette==4)? "checked":"" ;

      //drink
      $patientsHistory->dr1 = ($patientsHistory->Drink==1)? "checked":"" ;
      $patientsHistory->dr2 = ($patientsHistory->Drink==2)? "checked":"" ;
      $patientsHistory->dr3 = ($patientsHistory->Drink==3)? "checked":"" ;
      $patientsHistory->dr4 = ($patientsHistory->Drink==4)? "checked":"" ;



        $vill = Village::orderBy('village')->get();
    return View::make('patientEdit')
                ->with('patients',$patients)
                ->with('patientsHistory',$patientsHistory)
                ->with('maxTime',$maxTime)
                ->with('villcode',$vill);
  }

  public function editpatient($id)
  {
    $io = Input::all();

    $get_array = explode('-', $io["birthday"]);
    $day = $get_array[0];
    $month = $get_array[1];
    $year = $get_array[2]-543;

    $patient = Patient::find($id);

    $Patienthistory = Patienthistory::where('Time',$io["time"])->where('patient_id',$id)->first();
    if(empty($Patienthistory))
    {
      $Patienthistory = new Patienthistory;
      $Patienthistory->patient_id = $patient->id;
      $Patienthistory->Time = $io["time"];
    }




    $patient->prefix = $io["prefix"];
    $patient->Firstname = $io["firstname"];
    $patient->Sirname = $io["lastname"];
    $patient->CitizenID = str_replace("-","",$io["idcard"]);
    $patient->gender = $io["gender"];
    $patient->Birthday = $year.'-'.$month.'-'.$day ;
    $Patienthistory->Weight = $io["weight"];
    $Patienthistory->Height = $io["height"];
    $patient->BloodType = $io["bloodtype"];
    $patient->HomeNo = $io["homenum"];


    if(isset($io['moo']))
      $patient->Village = $io["moo"];

    if(isset($io['soi']))
      $patient->Alley = $io["soi"];

    if(isset($io['road']))
      $patient->Road = $io["road"];


    $patient->Subdistrict = $io["place"];
    $patient->District = Config::get('app.district');
    $patient->Province = Config::get('app.province');
    $patient->PostalCode = $io["postnum"];

    if(isset($io['phone']))
      $patient->Telephone = $io["phone"];
    if(isset($io['job']))
      $patient->Job = $io["job"];
    if(isset($io['Sickhistory']))
      $Patienthistory->Pastillness = $io["Sickhistory"];
    if(isset($io['Surgeryhistory']))
      $Patienthistory->Historysurgery = $io["Surgeryhistory"];
    if(isset($io['congenital']))
      $Patienthistory->Congenital = $io["congenital"];
    if(isset($io['Drug']))
      $Patienthistory->Drugidentification = $io["Drug"];
    if(isset($io['Allergy']))
      $Patienthistory->Allergic = $io["Allergy"];

    $Patienthistory->Cigarette = $io["cigarette"];


    if(isset($io['yearUseto']))
      $Patienthistory->CigaretteTime = $io["yearUseto"];

    $Patienthistory->Drink = $io["alcohol"];


    if(isset($io['alcoholuseto']))
      $Patienthistory->Drinktime = $io["alcoholuseto"];


    $get_array = explode('-', $io["submitday"]);
    $day = $get_array[0];
    $month = $get_array[1];
    $year = $get_array[2]-543;
    $Patienthistory->datesubmit = $year.'-'.$month.'-'.$day ;

    $Patienthistory->save();
    $patient->save();

    return Redirect::to('/patient/'.$patient->id.'/edit')->with('message', 'แก้ไข ผู้ป่วย : '.$io['firstname'].' สำเร็จ');
  }
  public function delpatient($id)
  {
    $patient = Patient::find($id);
    $result = ResultEvo::where('patient_id',$patient->id)
                         ->get();
   if($result->count()>0){
        return Redirect::to('/patient')->with('error', 'ไม่สามารถลบได้เนื่องจากมีข้อมูลทั้งหมด '.$result->count().' แถว');
    }
    $patient->delete();
    return Redirect::to('/patient')->with('message', 'ลบสมาชิกสำเร็จ');
  }

}
