<?php

class RecordController extends \BaseController {

  public function index($id,$form_id)
  {
    $io = Input::all();
    $isYears = isset($io['years'])&&$io['years']!="";
    $isTime = isset($io['times'])&&$io['times']!="";
    $page = 10;
    $form = Forms::find($form_id);
    $patient = Patient::find($id);


    $time = RecordTime::orderBy('record_years','DESC')->orderBy('record_times','DESC');
    if($isYears)
      $time = $time->where('record_years',$io['years']);
    if($isYears&&$isTime)
      $time = $time->where('record_times',$io['times']);
    $time = $time->paginate($page);

    $years = RecordTime::groupBy('record_years')->orderBy('record_years','DESC')->get();
    $times = array();
    if($isYears)
      $times =RecordTime::where('record_years',$io['years'])->orderBy('record_years','DESC')->get();

    return View::make('recordReport',[

                                      'form'=>$form,
                                      'id'=>$id,
                                      'form_id'=>$form_id,
                                      'patients'=>$patient,
                                      'time'=>$time,
                                      'years'=>$years,
                                      'times'=>$times,
                                      'io_times'=>(isset($io['times']))?$io['times']:"",
                                      'io_years'=>(isset($io['years']))?$io['years']:"",
                                    ]);

  }


  public function getAdd($id,$form_id,$years,$time)
  {
    $form = Forms::find($form_id);
    return View::make('reportRecordAdd')->with('form', $form)->with('time',$time)->with('years',$years);
  }

  public function postAdd($id,$form_id,$years,$times)
  {

    $io = Input::all();

    if (!RecordTime::isRecordTime($years,$times))
    {
      return Redirect::to('patient/'.$id.'/record/'.$form_id)
                       ->with('error', 'ไม่พบข้อมูลครั้งที่บันทึก');
    }

    $result = ResultEvo::where('patient_id',$id)
                        ->where('form_id', $form_id)
                        ->where('years',$years)
                        ->where('times',$times)
                        ->first();
    if(!isset($result))
      $result = new ResultEvo;
    else
      return Redirect::to('patient/'.$id.'/record/'.$form_id)
                      ->with('error', 'ไม่สามารถเพิ่มข้อมูลใหม่ได้เนื่องจากมีข้อมูลเก่าอยู่เเล้ว');

    $result->years = $years;
    $result->times = $times;
    $result->patient_id = $id;
    $result->form_id = $form_id;
    $result->save();

    unset($io['date']);
    $score = 0 ;
    foreach ($io as $key => $value) {
      $score  += $value ;
      $resultDetail = new ResultDetail;
      $resultDetail->evolu_id = $result->id;
      $evo_score = Score::where('evoluation_topic_topic_id','=',$key)->where('score','=',$value)->first();
      if(!isset($evo_score))
        return Redirect::to('patient/'.$id.'/record/'.$form_id.'/add')->with('error', 'คะแนนผิดพลาด');
      $resultDetail->score_id = $evo_score->id;
      $resultDetail->save();
    }

    $resultDescs_now = NULL;
    $resultDescs  = ResultDesc::where('form_id',$form_id)->get();

    foreach ($resultDescs as $rDesc) {
      if($rDesc->score_end==0)
      {
        if($score >= $rDesc->score_start)
          $resultDescs_now = $rDesc;
      }
      else{
        if($score >= $rDesc->score_start && $score <= $rDesc->score_end)
          $resultDescs_now = $rDesc;
      }

    }

    if(isset($resultDescs_now))
    {
      $result->result_id = $resultDescs_now->id;
      $result->result_name = $resultDescs_now->name;
    }

    $result->score = $score;
    $result->save();
    return Redirect::to('patient/'.$id.'/record/'.$form_id)->with('message', 'บันทึกข้อมูลสำเร็จ');

  }

  public function delResult($id,$form_id,$result_id)
  {
    $result = ResultEvo::find($result_id);
    $result->delete();
    return Redirect::to('patient/'.$id.'/record/'.$form_id)->with('message', 'ลบข้อมูลสำเร็จ');
  }

  public function getEdit($id,$form_id,$result_id)
  {
    $form = Forms::find($form_id);
    $result = ResultEvo::find($result_id);
    $time = $result->times;
    $years = $result->years;
    return View::make('reportRecordEdit')->with('form', $form)->with('result', $result)->with('time',$time)->with('years',$years);
  }
  public function postEdit($id,$form_id,$result_id)
  {
    $io = Input::all();

    $result = ResultEvo::find($result_id);
    $result->patient_id = $id;
    $result->form_id = $form_id;
    $result->save();

    unset($io['date']);
    $result->resultDetail()->delete();
    $score = 0 ;
    foreach ($io as $key => $value) {
      $score  += $value ;
      $resultDetail = new ResultDetail;
      $resultDetail->evolu_id = $result->id;
      $evo_score = Score::where('evoluation_topic_topic_id','=',$key)->where('score','=',$value)->first();
      if(!isset($evo_score))
        return Redirect::to('patient/'.$id.'/record/'.$form_id.'/add')->with('error', 'คะแนนผิดพลาด');
      $resultDetail->score_id = $evo_score->id;
      $resultDetail->save();
    }

    $resultDescs_now = NULL;
    $resultDescs  = ResultDesc::where('form_id',$form_id)->get();

    foreach ($resultDescs as $rDesc) {
      if($rDesc->score_end==0)
      {
        if($score >= $rDesc->score_start)
          $resultDescs_now = $rDesc;
      }
      else{
        if($score >= $rDesc->score_start && $score <= $rDesc->score_end)
          $resultDescs_now = $rDesc;
      }

    }

    if(isset($resultDescs_now))
    {
      $result->result_id = $resultDescs_now->id;
      $result->result_name = $resultDescs_now->name;
    }

    $result->score = $score;
    $result->save();
    return Redirect::to('patient/'.$id.'/record/'.$form_id)->with('message', 'แก้ไขข้อมูลสำเร็จ');
  }
}
