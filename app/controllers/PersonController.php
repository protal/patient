<?php

  class PersonController extends BaseController
  {
    public function index()
    {
      $page = 10;
      $person = Person::orderBy('pid','asc');
      if(!empty(Input::get('q')))
      {
        $q = Input::get('q');
        $person = $person->where('fname','like', '%'.$q.'%')
                    ->orwhere('lname', 'like', '%'.$q.'%');
      }
      else{
        $q = null;
      }
      $person = $person->paginate($page);
      return View::make('admin.person')
                   ->with('persons',$person)
                   ->with('q',$q);
    }

    public function addPersonView()
    {
      $vill = Village::orderBy('village')->get();
      return View::make('admin.personAdd' , ['villcode' => $vill ] );
    }

    public function addPerson()
    {
      $io = Input::all();
      $person = new Person;

      if (Person  ::where('fname', '=', $io['fname'])->count() > 0) {
         // person found
         if (Person  ::where('lname', '=', $io['lname'])->count() > 0)  {
           return Redirect::to('admin/person/add')->with('error', 'อสม : '.$io['fname'].' '.$io['lname'].' ไม่สามารถเพิ่มได้ เพราะอยู่ในระบบเเล้ว')->withInput();
         }
      }

      $person->fname = $io['fname'];
      $person->lname = $io['lname'];
      
      $villcode = House::endcodeVillcode($io['villcode']);
      $listHomeNo = explode(",",$io['hno']);
      
      $tt = House::where('villcode', '=', $villcode)
      ->whereIn('hno', $listHomeNo)->whereNotNull('pidvola')->get();
        if($tt->count() > 0){
            $error = "บ้านเลขที่ ";
            foreach ($tt as $index => $value){
                
                $error .= $value->hno;
                if($index < $tt->count()-1){
                    $error .= ", ";
                }
            }
            $error .= " มี อสม. แล้ว";
            return Redirect::to('admin/person/add')->with('error', $error)->withInput();
        }
        DB::transaction(function()
            use ($person, $listHomeNo, $villcode)
            {
                $person->save();
                
                foreach ($listHomeNo as $hno){
                    $home = House::where('villcode', '=', $villcode)
                            ->where('hno', $hno)->first();
                    if($home == null){
                        $home = new House();
                        $home->villcode = $villcode;
                        $home->hno = $hno;
                        $home->xgis = null;
                        $home->ygis = null;
                    }
                    
                    $home->pidvola = $person->pid;
                    $home->save();
                }
            }
        );
      
      return Redirect::to('admin/person')->with('message',' อสม. '.$io['fname']. ' สำเร็จ');
    }

    public function editpersonView($pid)
    {
      $person = Person::find($pid);
      if (empty($person))
        return Redirect::to('admin/person');

      $house = House::where('pidvola', $pid)->orderBy('hno')->first();
      $houseList = House::where('pidvola', $pid)->orderBy('hno')->get();
      $strHouseList = "";
      $arrHouseList = [];
      if($houseList->count() > 0){
          foreach ($houseList as $index => $value){
              $home = [];
              $home['homeNo'] = $value->hno;
              $home['lat'] = $value->ygis;
              $home['lng'] = $value->xgis;
              $arrHouseList[] = $home;
              $strHouseList .= $value->hno;
              if($index < $houseList->count()-1){
                  $strHouseList .= ", ";
              }
          }
      }

      $vill = Village::orderBy('village')->get();
      if(isset($house))
        $villhouse = $house->villcode[6].$house->villcode[7];
      else
        $villhouse = '';

      return View::make('admin.personEdit')
                  ->with('person',$person)
                  ->with('house',$house)
                  ->with('villcode',$vill)
                  ->with('villhouse',$villhouse )
                  ->with('strHouseList', $strHouseList);

    }

    public function editperson($pid)
    {
      $io = Input::all();
      $person = Person::find($pid);
      $house = House::where('pidvola', $pid)->orderBy('hno')->first();
      $villcode = House::endcodeVillcode($io['villcode']);
      $isChangeVillcode = false;
      if($house->villcode !== $villcode)
          $isChangeVillcode = true;

      if (empty($person)) return Redirect::to('admin/person');
      $person->fname = $io["fname"];
      $person->lname = $io["lname"];
      
      $listHomeNo = explode(",",$io['hno']);
      
      $tt = House::where('villcode', '=', $villcode)
      ->whereIn('hno', $listHomeNo)->where('pidvola', '!=', $pid)->get();
      if($tt->count() > 0){
          $error = "บ้านเลขที่ ";
          foreach ($tt as $index => $value){
              
              $error .= $value->hno;
              if($index < $tt->count()-1){
                  $error .= ", ";
              }
          }
          $error .= " มี อสม. แล้ว";
          return Redirect::to('admin/person/edit/'.$pid)->with('error', $error)->withInput();
      }
      
      DB::transaction(function()
          use ($person, $listHomeNo, $villcode, $pid, $isChangeVillcode)
          {
              $person->save();
              if($isChangeVillcode){
                  $changeHome = House::where('pidvola', $pid)->get();
                  foreach ($changeHome as $hno){
                      $hno->pidvola = null;
                      $hno->xgis = null;
                      $hno->ygis = null;
                      $hno->save();
                  }
              }
              
              foreach ($listHomeNo as $hno){
                  $home = House::where('villcode', '=', $villcode)
                  ->where('hno', $hno)->first();
                  if($home == null){
                      $home = new House();
                      $home->villcode = $villcode;
                      $home->hno = $hno;
                      $home->xgis = null;
                      $home->ygis = null;
                  }
                  
                  $home->pidvola = $person->pid;
                  $home->save();
              }
              
              $delHome = House::where('villcode', '=', $villcode)
              ->where('pidvola', $pid)->whereNotIn('hno', $listHomeNo)->get();
              
              foreach ($delHome as $hno){
                  $home = House::where('id', $hno->id)->first();
                  $home->pidvola = null;
                  $home->save();
              }
      });
      
      return Redirect::to('admin/person')->with('message', ' อสม. '.$io['fname']. ' สำเร็จ');
    }

    public function delperson($pid)
    {
      $person = Person::find($pid);
      $house = House::where('pidvola', $pid)->orderBy('hno')->first();

      if (isset($person) && ($house)) {
        if (isset($house->pidvola)) {
          return Redirect::to('admin/person')->with('error', ' ไม่สามารถลบได้เพราะมีบ้านที่รับผิดชอบ');
        }
      }
      $person->delete();
      return Redirect::to('admin/person')->with('message', 'ลบข้อมูล อสม. สำเร็จ');
    }

  }


 ?>
