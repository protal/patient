<?php

class UserController extends \BaseController {

  public function index()
  {
    $page = 10;
    $users = User::whereNull('deleted_at');
    if(!empty(Input::get('q')))
    {
      $q = Input::get('q');
      $users = $users
                    ->where(function($query)use($q){
                        $query->where('username', 'like', '%'.$q.'%')
                        ->orwhere('firstname', 'like', '%'.$q.'%')
                        ->orwhere('lastname', 'like', '%'.$q.'%');
                    })
                    ->paginate($page);
      return View::make('admin.user')->with('users',$users)->with('q',$q);
    }
    else{
        $users = $users->paginate($page);
        return View::make('admin.user')->with('users',$users) ;
    }


  }
  public function logout()
  {
    Auth::logout();
    return Redirect::to('login')->with('message', 'ออกจากระบบสำเร็จ' );
  }
  public function loginView()
  {
    return View::make('login');
  }
  public function login()
  {
    $io = Input::all();
    if(isset($io['remember'])&&$io['remember']==1)
      $remember = true;
    else
      $remember = false;
    if (Auth::attempt(array('username' => $io['username'], 'password' => $io['password']),$remember))
    {
        $log = new Loginlogs;
        $log->user_id = Auth::user()->id;
        $log->ip  = Request::getClientIp();
        $log->save();
        return Redirect::intended('/');
    }
    else {
        return Redirect::to('login')->with('error', 'ข้าสู่ระบบไม่สำเร็จ: Username หรือ password ไม่ถูกต้อง' )->withInput();
    }

  }



  public function addView()
  {
    return View::make('admin.userAdd');
  }
  public function addUser()
  {
    $io = Input::all();

    if (User::where('username', '=', $io['username'])->count() > 0) {
       // user found
       return Redirect::to('admin/user/add')->with('error', 'username : '.$io['username'].' ไม่สามารถเพิ่มได้ เพราะอยู่ในระบบเเล้ว')->withInput();
    }


    $user = new User;
    $user->username = $io['username'];
    $user->password = Hash::make($io['password']);
    $user->firstname = $io['firstname'];
    $user->lastname = $io['lastname'];
    $user->type = $io['type'];
    $user->save();

    return Redirect::to('admin/user')->with('message', 'เพิ่ม username : '.$io['username'].' สำเร็จ');
  }

  public function editView($id)
  {
    $user = User::find($id);
    $user->zero = ($user->type==0)? "SELECTED":"" ;
    $user->one = ($user->type==1)? "SELECTED":"" ;
    return View::make('admin.userEdit')->with('user',$user);
  }

  public function editUser($id)
  {
    $io = Input::all();

    // check password same
    if(isset($io['password']))
    {
      if($io['password']!=$io['repassword'])
        return Redirect::to('admin/user/edit/'.$id)->with('error', 'แก้ไขไม่สำเร็จรหัสผ่านไม่ตรงกัน');
    }

    $user = User::find($id);

    if(isset($io['password']))
      $user->password = Hash::make($io['password']);

    $user->firstname = $io['firstname'];
    $user->lastname = $io['lastname'];
    $user->type = $io['type'];

    $user->save();

    return Redirect::to('admin/user/edit/'.$id)->with('message', 'แก้ไข สมาชิก : '.$io['firstname'].' สำเร็จ');
  }

  public function delUser($id)
  {
    $user = User::find($id);
    $user->delete();
    return Redirect::to('admin/user/')->with('message', 'ลบสมาชิกสำเร็จ');
  }



}
