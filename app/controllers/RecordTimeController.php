<?php
class RecordTimeController extends BaseController {

  public function index()
  {
    $page = 10;
    $recordTime = RecordTime::orderBy('record_years','desc') ;
    if(!empty(Input::get('q')))
    {
      $q = Input::get('q');
      $recordTime = $recordTime->where('record_years','like', '%'.$q.'%')
                  ->orwhere('record_times', 'like', '%'.$q.'%');
    }
    else{
      $q = null;
    }
    $recordTime = $recordTime->paginate($page);
    return View::make('admin.recordTime')
               ->with('recordTimes',$recordTime)
               ->with('q',$q);
  }
  public function timeToYear($year)
  {
    $recordTime = RecordTime::where('record_years',$year)->orderBy('record_times','desc')->get();

    $times = array();
    foreach ($recordTime as $record) {
      $times[] = $record->record_times;
    }

    return Response::json($times);
  }
  public function addrecordTimeView()
  {
    return View::make('admin.recordTimeAdd');
  }

  public function addrecordTime()
  {
    $io = Input::all();
    if (RecordTime::where('record_years', $io['ryear'])->where('record_times', $io['rtime'])->count() > 0) {
       return Redirect::to('admin/recordTime/add')->with('error', 'ปีที่บันทึก '.$io['ryear']. ' และครั้งที่ '.$io['rtime'].' ไม่สามารถเพิ่มได้ เพราะมีอยู่ในระบบเเล้ว')->withInput();
    }
    $recordTime = new RecordTime;
    $recordTime->record_years = $io['ryear'];
    $recordTime->record_times = $io['rtime'];
    $recordTime->save();
    return Redirect::to('admin/recordTime')->with('message', 'เพิ่มปีที่ '.$io['ryear']. ' และครั้งที่ '.$io['rtime']. ' สำเร็จ');
  }

  public function editrecordTimeView($id)
  {
    $recordTime = RecordTime::find($id);
    return View::make('admin.recordTimeEdit')->with('recordTime',$recordTime);
  }

  public function editrecodeTime($id)
  {
    $io = Input::all();
    $recordTime = RecordTime::find($id);
    $exitsRecord = RecordTime::where('record_years', $io['ryear'])
                              ->where('record_times', $io['rtime'])
                              ->where('id','!=',$id)
                              ->count()  > 0;
    if ($exitsRecord) {
       return Redirect::to('admin/recordTime/edit/'.$id)->with('error', 'ปีที่บันทึก '.$io['ryear']. ' และครั้งที่ '.$io['rtime'].' ไม่สามารถแก้ไขได้ เพราะมีอยู่ในระบบเเล้ว')->withInput();
    }
    if (empty($recordTime))
      return Redirect::to('admin/recordTime');
    $recordTime->record_years = $io["ryear"];
    $recordTime->record_times = $io["rtime"];
    $recordTime->save();
    return Redirect::to('admin/recordTime')->with('message', 'แก้ไขปีที่บันทึก '.$io['ryear'].' และครั้งที่ '.$io['rtime'].' สำเร็จ');

  }

  public function delrecodeTime($id)
  {
    $recordTime = RecordTime::find($id);

    $result = ResultEvo::where('years',$recordTime->record_years)
                         ->where('times',$recordTime->record_times)
                         ->get();


    if($result->count()>0){
        return Redirect::to('admin/recordTime')->with('error', 'ไม่สามารถลบได้เนื่องจากมีข้อมูลทั้งหมด '.$result->count().' แถว');
    }

    if (isset($recordTime)) {
      $recordTime->delete();
    }
    return Redirect::to('admin/recordTime')->with('message', 'ลบปีที่บันทึกและครั้งที่บันทึกสำเร็จ');
  }

}
?>
