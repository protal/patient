<?php

class ApiController extends BaseController {
    public function vilToVol($vilcode)
    {
        $villcode = House::endcodeVillcode($vilcode);
        $house = House::where('villcode',$villcode)->get();
        $pidvola = [];
        foreach($house as $h)
            $pidvola[] = $h['pidvola'];
        $persons = Person::whereIn('pid',$pidvola)->get()->toArray();
        return $persons;

    }
}

