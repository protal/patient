<?php

class GraphController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index()
	{
		$parts = Part::all() ;
		return View::make('graph')->with('parts',$parts);
	}
	public function result($part,$id)
	{
		//search input
		$io = Input::all();
		$isYears = isset($io['years'])&&$io['years']!="";
		$isTimes = isset($io['times'])&&$io['times']!="";
		$isVillage = isset($io['village'])&&$io['village']!="";
		$isVolunteer = isset($io['volunteer'])&&$io['volunteer']!="";

		//find
		$inputVillage = ($isVillage) ? $io['village']:"1";
		$villcode = House::endcodeVillcode($inputVillage);
		$house = House::where('villcode',$villcode)->get();
		$pidvola = [];
		foreach($house as $h)
			$pidvola[] = $h['pidvola'];
		$persons = Person::whereIn('pid',$pidvola)->get();
		$years = RecordTime::groupBy('record_years')->orderBy('record_years','desc')->get();
		$recordTimes = RecordTime::where('record_years',($isYears)?$io['years']:$years[0]->record_years)->orderBy('record_times','DESC')->get();
		// endfind

		$part = Part::find($part);
		$result_desc = ResultDesc::where('form_id',$id)->get();
		$patient_results = ResultEvo::where('form_id',$id);

		if($isYears&&$isTimes){
			$patient_results = $patient_results->where('times',$io['times'])->where('years',$io['years']);
		}
		else{
			$patient_results = $patient_results->where('times',$recordTimes[0]->record_times)->where('years',$years[0]->record_years);
		}

		$patient_results = $patient_results->groupBy('patient_id')->get();


		$rs = array();// result count
		$rsd = array();// result count drilldown women and men

		// group province , district , subdistrict , village
		$countProvince = array();
		$countDistrict = array();
		$countSubdistrict = array();
		$countVillage = array();

		$villages = Village::orderby('village')->get();
		foreach($villages as $v)
				$countVillage[] = $v['village'];

		sort($countVillage);

		foreach ($patient_results as $pr) {
			// add p d sd v in count
			$patient =  $pr->patient;
			if(!in_array($patient->Province,$countProvince)){
				$countProvince[] =  $patient->Province;
			}
			if(!in_array($patient->District,$countDistrict)){
				$countDistrict[] =  $patient->District;
			}
			if(!in_array($patient->Subdistrict,$countSubdistrict)){
				$countSubdistrict[] =  $patient->Subdistrict;
			}

			//check is not in village go next
			if($isVillage && $patient->Village != $io['village'])
				continue;
			//check vola
			if($isVolunteer)
				if(!$volun = $patient->volunteer())
					continue;
				else if($volun->pid != $io['volunteer'])
					continue;

			$r = ResultEvo:: where('form_id',$id)
											->where('patient_id',$pr->patient_id)
											->where('years',$pr->years)
											->where('times',$pr->times)
											->orderBy('date','DESC')
											->first();
			$rs[$r->result_id] = isset($rs[$r->result_id])?$rs[$r->result_id]+1:1;

			if($r->patient->gender == 1 or $r->patient->gender == 0)
				$rsd[$r->result_id][$r->patient->gender] = isset(
																	$rsd[$r->result_id][$r->patient->gender]
																)?
																$rsd[$r->result_id][$r->patient->gender]+1
																:1;
		}


		$series = array();
		$d_series = array();
		if(sizeof($rs)<1)
			return View::make('graphfind',
				[
					'error' => 1,
					'vola' => $persons,
					'years' => $years,
					'recordTimes' => $recordTimes,
					'village' => $countVillage,
					'q_village' => ($isVillage)?$io['village']:"",
					'q_years' => ($isYears)?$io['years']:"",
					'q_times' => ($isTimes)?$io['times']:"",
					'q_volunteer' => ($isVolunteer)?$io['volunteer']:"",
				]);

		foreach ($result_desc as $rd) {
			$t['name'] = $rd->name;
			$t['y'] = (isset($rs[$rd->id]))?$rs[$rd->id]:0;
			$t['drilldown'] = $rd->id;
			array_push($series,$t);

			$d_t['name'] = $rd->name;
			$d_t['id'] = $rd->id;
			$d_t['data'] = array(
									array(
											'เพศชาย',
											(isset($rsd[$rd->id][1])?$rsd[$rd->id][1]:0)
										),
									array(
											'เพศหญิง',
											(isset($rsd[$rd->id][0])?$rsd[$rd->id][0]:0)
										)
								);
			array_push($d_series,$d_t);
		}


		// $countProvince = array();
		// $temp  = array();
		// foreach($countProvince as $p)
		// {
		// 	$t = Cprovince::where('provcode',$p)->first();
		// 	$temp[] = ([
		// 		'name' => $t->provname,
		// 		'code' => $t->provcode
		// 	]);
		// }
		// $countProvince = $temp;



		$search_output = "";
	 	if($isVillage)
			$search_output .= "village=".$io['village'].'&';
		if($isVolunteer)
			$search_output .= "volunteer=".$io['volunteer'].'&';
		if($isYears)
			$search_output .= "years=".$io['years'].'&';
		if($isTimes)
			$search_output .= "times=".$io['times'].'&';





		return View::make('graphfind')
					->with('part',$part)
					->with('series',json_encode($series))
					->with('d_series',json_encode($d_series))
					->with('village',$countVillage)
					->with('q_village',($isVillage)?$io['village']:"")
					->with('q_years',($isYears)?$io['years']:"")
					->with('q_times',($isTimes)?$io['times']:"")
					->with('q_volunteer',($isVolunteer)?$io['volunteer']:"")
					->with('result_id',$id)
					->with('search_output',$search_output)
					->with('recordTimes',$recordTimes)
					->with('years',$years)
					->with('vola',$persons);
	}

	public function resultTable($part,$id,$resul_id,$gender)
	{

        //?village=1&years=2561&times=1&
        $io = Input::all();
        $isYears = isset($io['years'])&&$io['years']!="";
		$isTimes = isset($io['times'])&&$io['times']!="";
        $isVillage = isset($io['village'])&&$io['village']!="";
        $isVolunteer = isset($io['volunteer'])&&$io['volunteer']!="";


        $village = Input::get('village');
        if(!empty(Input::get('volunteer')))
            $volunteer = Input::get('volunteer');
        $IOyears = Input::get('years');
        $IOtimes = Input::get('times');

        $years = RecordTime::groupBy('record_years')->orderBy('record_years','desc')->get();
        $recordTimes = RecordTime::where('record_years',($isYears)?$io['years']:$years[0]->record_years)->orderBy('record_times','DESC')->get();

        $patient_results = ResultEvo::where('form_id',$id);
        if($isYears&&$isTimes){
            $patient_results = $patient_results->where('times',$IOtimes)->where('years',$IOyears);
        }
        else
            $patient_results = $patient_results->where('times',$recordTimes[0]->record_times)->where('years',$years[0]->record_years);

        $patient_results = $patient_results->groupBy('patient_id')->get();
        $patient_result = array();
		foreach ($patient_results as $pr) {

			$r = ResultEvo:: where('form_id',$id)->where('patient_id',$pr->patient_id)->orderBy('date','DESC')->first();

            if($r->result_id == $resul_id)
			{
                $patient =  $pr->patient;

				if($isVillage)
				{
					if($patient->Village!=$village)
					{
						continue;
                    }
                }
                if($isVolunteer)
                    if(!$volun = $patient->volunteer())
                        continue;
                    else
                        if($volun->pid != $volunteer)
                            continue;
				if($patient->gender == $gender)
				{
					$patient_result[] = $patient;
				}
            }

        }
		return View::make('graphtable')->with('patient_result',$patient_result);

	}


}
