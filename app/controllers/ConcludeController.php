<?php

class ConcludeController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index()
	{
		$parts = Part::all() ;
		return View::make('conclude')->with('parts',$parts);
	}
	public function result($part,$id)
	{
		//search input
	    $village = Input::get('village');
	    $year = Input::get('year');
	    
	    $recordYear = AppDefault::getRecordTime();
	    $villages = AppDefault::getArrayVillage();
	    $village = ($village==null)?$villages[0]->village:$village;
	    $year = ($year==null)?$recordYear[0]->record_years:$year;
	    
	    $havePatient = AppDefault::isPatientInForm($year, $id);
	    $error = 0;
	    if(!$havePatient){
	       $error = 1;
	    }
	    
	    $part = Part::find($part);
	    $result_desc = ResultDesc::where('form_id',$id)->get();
// 	    $patient_results = ResultEvo::where('form_id',$id);
	    
	    $arrResult = array();
	    foreach($result_desc as $desc){
	        array_push($arrResult, $desc->name);
	    }
	    
	    $result = AppDefault::findResultInForm($year, $id, $village);
		
	    return View::make('concludefind')->with('recordYear', $recordYear)->with('villages', $villages)->with('village', $village)
	    ->with('year', $year)->with('arrResult', json_encode($arrResult))->with('part', $part)->with('error',$error)->with('result', json_encode($result));
	}

	public function resultTable($formId, $village, $year, $time, $result)
	{
	    $desc = DB::table('result_desc')
	    ->where('name', "=", $result)->first();
	    
	    $result = DB::table('evoluation_result')
	    ->join('patient','patient.id', '=', 'evoluation_result.patient_id')
	    ->where('form_id', "=", $formId)
	    ->where('years', "=", $year)
	    ->where('times', "=", $time)
	    ->where('village', "=", $village)
	    ->where('result_id', "=", $desc->id)
	    ->orderby('prefix', 'desc')->get();
	    
	    $volunteer = AppDefault::getAllVolunteer($village);
	    $arrVolunteer = [];
	    foreach ($volunteer as $volun){
	        $arrVolunteer[$volun->hno] = $volun->fname." ".$volun->lname;
	    }
	   
	    
	    $patient_result = [];
	    foreach ($result as $val){
	        $val->age = AppDefault::calAge($val->Birthday);
	        try {
	           $val->volunteer = $arrVolunteer[$val->HomeNo];
	        }catch (Exception $e){
	            $val->volunteer = "";
	        }
	    }

	    return View::make('concludetable')->with('patient_result',$result);;

	}


}
