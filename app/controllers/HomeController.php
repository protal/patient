<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index($vid)
	{
	    $page = 10;
	    $villcode = House::endcodeVillcode($vid);
	    $home = House::where('villcode', '=', $villcode)->orderBy('hno','asc');
	    if(!empty(Input::get('q')))
	    {
	        $q = Input::get('q');
	        $home = $home->where('hno','like', '%'.$q.'%')
	        ->where('villcode', '=', $villcode);
	        
	    }
	    else{
	        $q = null;
	    }
	    $home = $home->paginate($page);
	    
	    return View::make('admin.home')
	       ->with('homes',$home)
	       ->with('q',$q)
	       ->with('vid', $vid);
	}
	
	public function addhomeView($vid)
	{
	    return View::make('admin.homeAdd')->with('vid',$vid);
	}
	
	public function addhome($villcode){
	    $io = Input::all();
	    $vid = $io['vid'];
	    $arrHno = $io['hno'];
	    $latitude = $io['latitude'];
	    $longtitude = $io['longtitude'];
	    $tt = House::where('villcode', '=', House::endcodeVillcode($villcode))
	    ->whereIn('hno', $arrHno)->get();
	    if($tt->count() > 0){
	        $error = "บ้านเลขที่ ";
	        foreach ($tt as $index => $value){
	            $error .= $value->hno;
	            if($index < $tt->count()-1){
	                $error .= ", ";
	            }
	        }
	        $error .= " มีอยู่แล้ว";
	        return Redirect::to('admin/village/home/add/'.$vid)->with('error', $error)->withInput();
	    }
	    
	    
	    foreach ($arrHno as $index => $hno){
	        $home = new House();
	        $home->pidvola = null;
	        $home->hno = $hno;
	        $home->villcode = House::endcodeVillcode($vid);
	        $home->xgis = empty($longtitude[$index])?null:$longtitude[$index];
	        $home->ygis = empty($latitude[$index])?null:$latitude[$index];
	        $home->save();
	    }
	    
	    return Redirect::to('admin/village/home/'.$vid)->with('message', 'เพิ่มบ้านเลขที่สำเร็จ');
	}
	
	public function delhome($vid,$homeid)
	{
	    $house = House::find($homeid);
	    
	    $house->delete();
	    return Redirect::to('admin/village/home/'.$vid)->with('message', 'ลบข้อมูล บ้านเลขที่ สำเร็จ');
	}
	
	public function edithomeView($villcode, $homeId){
	    $house = House::find($homeId);
	    return View::make('admin.homeEdit')
	       ->with("vid", $villcode)->with("homeId", $homeId)->with("home", $house);
	}
	
	public function edithome($vid, $homeId){
	    
	    $io = Input::all();
	    $villcode = House::endcodeVillcode($vid);
	    $home = House::where('villcode', '=', $villcode)
	       ->where('hno', '=', $io["hno"])
	       ->where('id', '!=', $homeId)->first();
        if($home != null){
            return Redirect::to('admin/village/home/edit/'.$vid."/".$homeId)->with('error', "บ้านเลขที่ ".$io["hno"]." ไม่สามารถเพิ่มได้ เพราะอยู่ในระบบเเล้ว")->withInput();
        }
        
	    $house = House::find($homeId);
		$house->hno = $io["hno"];
		$house->xgis = empty($io["latitude"])?null:$io["latitude"];
	    $house->ygis = empty($io["longtitude"])?null:$io["longtitude"];
	    $house->save();
	    return Redirect::to('admin/village/home/'.$vid)->with('message', 'แก้ไขบ้านเลขที่สำเร็จ');
	}

}
