<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Route::get('/database', 'DBController@index');
// Route::get('/insert', 'DBController@insert7');


//Auth
Route::get('/login', 'UserController@loginView');
Route::post('/login', 'UserController@login');

Route::get('/logout', 'UserController@logout');

//end Auth



Route::group(array('before'=>'auth'), function()
{
	Route::get('/', ['as' => 'home',function()
	{
		return Redirect::to('patient');
	}]);

	Route::get('graph', ['uses' => 'GraphController@index', 'as' => 'graph']);
	Route::get('graph/{part}/{id}',['uses' => 'GraphController@result', 'as' => 'graph.result']);
	Route::get('graph/{part}/{id}/{resul_id}/{gender}','GraphController@resultTable');
	
	Route::get('conclude', ['uses' => 'ConcludeController@index', 'as' => 'conclude']);
	Route::get('conclude/{part}/{id}',['uses' => 'ConcludeController@result', 'as' => 'conclude.result']);
	Route::get('conclude/{formId}/{vilage}/{year}/{time}/{result}','ConcludeController@resultTable');

	//patient
	Route::get('/patient/{id}/report',['uses' => 'PatientReportController@index', 'as' => 'patient.report']);

	Route::get('patient/{id}/record/{form_id}',['uses' => 'RecordController@index', 'as' => 'patient.record']);
	Route::get('patient/{id}/record/{form_id}/add/{years}/{time}',['uses' => 'RecordController@getAdd', 'as' => 'patient.record.add']);
	Route::post('patient/{id}/record/{form_id}/add/{years}/{time}', 'RecordController@postAdd');

	Route::get('patient/{id}/record/{form_id}/result/{result_id}/edit',['uses' => 'RecordController@getEdit', 'as' => 'patient.record.edit']);
	Route::post('patient/{id}/record/{form_id}/result/{result_id}/edit', 'RecordController@postEdit');
	// reult
	Route::get('patient/{id}/record/{form_id}/result/{result_id}/delete', 'RecordController@delResult');


  //patient
	Route::get('/patient', ['uses' => 'PatientController@index', 'as' => 'patient']);

	// patientAdd
	Route::get('/patient/add', ['uses' => 'PatientController@addpatientView', 'as' => 'patient.add']);
	Route::post('/patient/add', 'PatientController@addpatient');
	//patientedit
	Route::get('/patient/{id}/edit', ['uses' => 'PatientController@editpatientView', 'as' => 'patient.edit']);
	Route::post('/patient/{id}/edit', 'PatientController@editpatient');
	Route::get('/patient/{id}/delete', 'PatientController@delpatient');


	//recordtim API TIME TO years
    Route::get('/api/recordtime/year/{year}','RecordTimeController@timeToYear');

    Route::get('/api/villtovol/{vilcode}','ApiController@vilToVol');

});



// admin
Route::group(array('prefix' => 'admin','before'=>'auth|admin'), function()
{

	Route::get('/user', ['uses' => 'UserController@index', 'as' => 'admin.user']);
	Route::get('/user/add', ['uses' => 'UserController@addView', 'as' => 'admin.user.add']);
	Route::post('/user/add', 'UserController@addUser');
	Route::get('/user/edit/{id}', ['uses' => 'UserController@editView', 'as' => 'admin.user.edit']);
	Route::post('/user/edit/{id}', 'UserController@editUser');
	Route::get('/user/delete/{id}', 'UserController@delUser');


	Route::get('/report', ['uses' => 'ReportManageController@index', 'as' => 'admin.report']);
	Route::get('/report/part/add', ['uses' => 'ReportManageController@partAddView', 'as' => 'admin.report.part.add']);
	Route::post('/report/part/add', 'ReportManageController@partAdd');
	Route::get('/report/part/{id}/delete', 'ReportManageController@partDel');
	Route::get('/report/part/{id}', [ 'uses' => 'ReportManageController@partEditView' ,  'as' => 'admin.report.part.edit']);
	Route::post('/report/part/{id}', 'ReportManageController@partEdit');

	Route::get('/report/form/{id}', ['uses' => 'ReportManageController@formView' , 'as' => 'admin.report.form']);
	// Route::get('/report/part/{id}/form/add', 'ReportManageController@formAddView');
	Route::get('/report/part/{id}/form/add', ['uses' => 'ReportManageController@formAddView' , 'as' => 'admin.report.part.form.add']);
	Route::post('/report/part/{id}/form/add', 'ReportManageController@formAdd');
	Route::get('/report/form/{id}/edit', ['uses' => 'ReportManageController@formEditView', 'as' => 'admin.report.form.edit']);
	Route::post('/report/form/{id}/edit', 'ReportManageController@formEdit');

	Route::get('/report/form/{id}/delete', 'ReportManageController@formDel');


	//topicadd
	Route::get('/report/form/{id}/topic/add', ['uses' => 'ReportManageController@topicAddView' , 'as' => 'admin.report.form.topic.add']);
	Route::post('/report/form/{id}/topic/add', 'ReportManageController@topicAdd');

	// topicedit
	Route::get('/report/topic/{id}/edit',  ['uses' => 'ReportManageController@topicEditView' , 'as' => 'admin.report.topic.edit']);
	Route::post('/report/topic/{id}/edit', 'ReportManageController@topicEdit');

	Route::get('/report/topic/{id}/delete', 'ReportManageController@topicDel');



	//topicadd
	Route::get('/report/form/{id}/result/add' , ['uses' =>  'ReportManageController@resultAddView' , 'as' => 'admin.report.form.result.add']);
	Route::post('/report/form/{id}/result/add', 'ReportManageController@resultAdd');

	Route::get('/report/result/{id}/edit', ['uses' => 'ReportManageController@resultEditView', 'as' => 'admin.report.result.edit']);
	Route::post('/report/result/{id}/edit', 'ReportManageController@resultEdit');

	Route::get('/report/result/{id}/delete', 'ReportManageController@resultDel');

	//recordTime
	Route::get('/recordTime', ['uses' => 'RecordTimeController@index', 'as' => 'admin.recordTime']);
	Route::get('/recordTime/add', ['uses' => 'RecordTimeController@addrecordTimeView', 'as' => 'admin.recordTime.add']);
	Route::post('/recordTime/add', 'RecordTimeController@addrecordTime');
    Route::get('/recordTime/edit/{id}', ['uses' => 'RecordTimeController@editrecordTimeView', 'as' => 'admin.recordTime.edit']);
	Route::post('/recordTime/edit/{id}', 'RecordTimeController@editrecodeTime');
	Route::get('/recordTime/delete/{id}', 'RecordTimeController@delrecodeTime');

	//village
	Route::get('/village', ['uses' => 'VillageController@index', 'as' => 'admin.village']);
	Route::get('/village/add', ['uses' => 'VillageController@addvillageView', 'as' => 'admin.village.add']);
	Route::post('/village/add', 'VillageController@addvillage');
  Route::get('/village/edit/{id}', ['uses' => 'VillageController@editvillageView', 'as' => 'admin.village.edit']);
	Route::post('/village/edit/{id}', 'VillageController@editvillage');
	Route::get('/village/delete/{id}', 'VillageController@delvillage');

	//person
	Route::get('/person', ['uses' => 'PersonController@index', 'as' => 'admin.person']);
	Route::get('/person/add', ['uses' => 'PersonController@addpersonView', 'as' => 'admin.person.add']);
	Route::post('/person/add', 'PersonController@addperson');
	Route::get('/person/edit/{id}', ['uses' => 'PersonController@editpersonView', 'as' => 'admin.person.edit']);
	Route::post('/person/edit/{pid}', 'PersonController@editperson');
	Route::get('/person/delete/{id}', 'PersonController@delperson');
	
	//home
	Route::get('/village/home/{vid}', ['uses' => 'HomeController@index', 'as' => 'admin.village.home']);
	Route::get('/village/home/add/{vid}', ['uses' => 'HomeController@addhomeView', 'as' => 'admin.village.home.add']);
	Route::post('/village/home/add/{villcode}', 'HomeController@addhome');
	Route::get('/village/home/edit/{villcode}/{homeId}', ['uses' => 'HomeController@edithomeView', 'as' => 'admin.village.home.add']);
	Route::post('/village/home/edit/{villcode}/{homeId}', 'HomeController@edithome');
	Route::get('/village/home/delete/{vid}/{homeid}', 'HomeController@delhome');
});

// map
Route::get('/health', 'HealthController@index');
Route::post('/health', 'HealthController@index');

Route::get('/form/{formId}', 'HealthController@evoluation');
Route::post('/form/{formId}', 'HealthController@evoluation');

Route::get('/behavior', 'HealthController@behavior');
Route::post('/behavior', 'HealthController@behavior');

Route::get('/volunteer', 'HealthController@volunteer');
Route::post('/volunteer', 'HealthController@volunteer');

Route::post('/getrecordtime', 'HealthController@getrecordtime');
