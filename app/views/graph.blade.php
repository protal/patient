@extends('layouts.master')
@section('title')
  กราฟสรุปบันทึกสุขภาพ
@stop

@section('body')
<?php $i = 0 ; ?>
@foreach ($parts as $part)
  <div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">
        <h5>
          ส่วนที่ {{++$i}} {{$part->name}}
        </h5>
    </div>
    @if($part->forms->count()==0)
    <div class="panel-body text-center">
      ...ไม่พบหัวข้อในการบันทึก...
    </div>

    @else
      <!-- Table -->
      <table class="table table-striped">
        <thead>
          <tr>
            <td width="75%">หัวข้อบันทึก</td>
            <td>ดำเนินการ</td>
          </tr>
        </thead>
        <tbody>
          @foreach ($part->forms as $form)
          <tr>
            <td>{{$form->name}}</td>
            <td>
              <a href="{{{url('graph/'.$part->id.'/'.$form->id)}}}" class="btn btn-primary  btn-sm"> <span class="fa fa-bar-chart" aria-hidden="true"> ดูผลสรุป</span></a>
            </td>
          </tr>
          @endforeach
        </tdbody>
      </table>
    @endif


  </div>
@endforeach


@stop
