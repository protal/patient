@extends('layouts.master')
@section('title')
  บันทึกสุขภาพ
@stop

@section('body')
<?php $i = 0 ; ?>
@foreach ($parts as $part)
<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">ส่วนที่ {{++$i}} {{$part->name}}</div>

  <!-- Table -->
  @if($part->forms->count()==0)
  <div class="panel-body text-center">
    ...ไม่พบหัวข้อในการบันทึก...
  </div>

  @else
  <table class="table table-striped">
    <thead>
      <tr>
        <td>#</td>
        <td width="60%">หัวข้อบันทึก</td>
        <td>ผลการประเมิน</td>
        <td>ดำเนินการ</td>
      </tr>
    </thead>
    <tbody>
      <?php $i = 0; ?>
      @foreach ($part->forms as $form)

      <tr>
        <td>{{++$i}}</td>
        <td>{{$form->name}}</td>
        <?php $result = $form->resultEvo()->where('patient_id',$patients->id)->orderBy('date', 'desc')->first() ?>
        <td>{{$result['result_name']}}</td>
        <td>
          <a href="{{{url('patient/'.$patients->id.'/record/'.$form->id)}}}" class="btn btn-default  btn-sm"> <span class="glyphicon glyphicon-pencil" aria-hidden="true"> ลงบันทึก</span></a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  @endif
</div>
@endforeach


@stop
