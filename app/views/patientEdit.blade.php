@extends('layouts.master')
@section('title')
  แก้ไขประวัติส่วนตัวผู้สูงอายุ
@stop
<?php
  $disabled = "disabled" ;
 ?>
 @section('css')

   <link href="{{ URL::asset('theme/tagsInput/src/jquery.tagsinput.css'); }}" rel="stylesheet">
   <style media="screen">
     div.tagsinput span.tag{
       background: #0e52a2;
       border: 1px solid #000610;
     }
   </style>
 @stop
@section('body')
    <div class="container">
      <div class="row">
        <form class="" action="" method="post" data-parsley-validate novalidate>
        <!-- panel 1 -->
        <div class="col-md-6">
          <div class="panel panel-default">
            <!-- Default panel1 contents -->
            <div class="panel-heading">
              <h5>แก้ไขประวัติส่วนตัวผู้สูงอายุ</h5>
            </div>
            <!-- Table1 -->
            <div class="panel-body">
              <!-- 2 form -->
              <div class="row">
                <label class="col-md-4 control-label" for="name" style="padding-top:5px;">คำนำหน้า</label>
                <div class="col-md-8">
                  <div class="form-group">
                    <select class="form-control prefix" name="prefix" required>
                      <option value="">--คำนำหน้า--</option>
                      <option value="นาย" {{$patients->preMr}}>นาย</option>
                      <option value="นาง" {{$patients->preMrs}}>นาง</option>
                      <option value="นางสาว" {{$patients->preMiss}}>นางสาว</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-4 control-label">
                  <label for="name" style="padding-top:5px;">ชื่อจริง</label>
                </div>
                <div class="col-md-8">
                  <div class="form-group">
                    <input type="text" class="form-control" name="firstname" placeholder="ชื่อจริง" value="{{$patients->Firstname}}" required>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-4 control-label">
                  <label for="lastname" style="padding-top:5px;">นามสกุล</label>
                </div>
                <div class="col-md-8">
                  <div class="form-group">
                    <input type="text" class="form-control" name="lastname" placeholder="นามสกุล" value="{{$patients->Sirname}}" required>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-4 control-label">
                  <label for="idcard" style="padding-top:5px;">เลขบัตรประชาชน</label>
                </div>
                <div class="col-md-8">
                  <div class="form-group">
                    <input type="text" class="form-control" name="idcard" placeholder="เลขบัตรประชาชน" value="{{$patients->CitizenID}}"   data-inputmask="'mask': '9-9999-99999-99-9'" required>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-4 control-label">
                  <label for="birthday" style="padding-top:5px;">วัน เดือน ปี เกิด</label>
                </div>
                <div class="col-md-8">
                  <div class="form-group">
                    <input type="text" class="form-control" name="birthday" value="{{$patients->Birthday()}}" placeholder="dd-mm-yyyy"  readonly="readonly" required>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-4 control-label">
                  <label for="sex" style="padding-top:5px;">เพศ</label>
                </div>
                <div class="col-md-8">
                  <div class="form-group">
                    <select class="form-control gender" name="gender" required readonly>
                      <option value="">------เพศ------</option>
                      <option value="1" {{$patients->genM}}>ชาย</option>
                      <option value="0" {{$patients->genW}}>หญิง</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="row">
                <label class="col-md-4 control-label" for="weight" style="padding-top:5px;">น้ำหนัก</label>
                <div class="col-md-8">
                  <div class="form-group">
                  <input type="text" class="form-control" name="weight" placeholder="น้ำหนัก" value="{{$patientsHistory->Weight}}" required>
                  </div>
                </div>
              </div>

              <div class="row">
                <label class="col-md-4 control-label" for="height" style="padding-top:5px;">ส่วนสูง</label>
                <div class="col-md-8">
                  <div class="form-group">
                  <input type="text" class="form-control" name="height" placeholder="ส่วนสูง" value="{{$patientsHistory->Height}}" required>
                  </div>
                </div>
              </div>

              <div class="row">
                <label class="col-md-4 control-label" for="bloodtype" style="padding-top:5px;">หมู่เลือด</label>
                <div class="col-md-8">
                  <div class="form-group">
                    <select class="form-control" name="bloodtype" required>
                      <option value="A" {{$patients->btA}}>A</option>
                      <option value="B" {{$patients->btB}}>B</option>
                      <option value="AB"{{$patients->btAB}}>AB</option>
                      <option value="O" {{$patients->btO}}>O</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-4 control-label">
                  <label for="job" style="padding-top:5px;">อาชีพ</label>
                </div>
                <div class="col-md-8">
                  <div class="form-group">
                    <input type="text" class="form-control" name="job" placeholder="อาชีพ" value="{{$patients->Job}}" required>
                  </div>
                </div>
              </div>

              <div class="row">
                <label class="col-md-4 control-label" for="homenum" style="padding-top:5px;">บ้านเลขที่</label>
                <div class="col-md-8">
                  <div class="form-group">
                  <input type="text" class="form-control" name="homenum" placeholder="บ้านเลขที่" value="{{$patients->HomeNo}}" required>
                  </div>
                </div>
              </div>

              <div class="row">
                <label class="col-md-4 control-label" for="moo" style="padding-top:5px;">หมู่ที่</label>
                <div class="col-md-8">
                  <div class="form-group">
                    <select class="form-control" name="moo" title="เลือกหมู่บ้าน" required>
                      <option value="">เลือกหมู่บ้าน</option>
                        @foreach($villcode as $v)
                            <option value="{{$v->village}}" {{($patients->Village==$v->village)? "SELECTED":""}}>{{$v->village}}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
              </div>

              <div class="row">
                <label class="col-md-4 control-label" for="soi" style="padding-top:5px;">ตรอก/ซอย</label>
                <div class="col-md-8">
                  <div class="form-group">
                    <input type="text" class="form-control" name="soi" placeholder="ตรอก/ซอย" value="{{$patients->Alley}}">
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="from-group">
                    <label for="road">ถนน</label>
                      <input type="text" class="form-control" name="road" placeholder="ถนน" value="{{$patients->Road}}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="from-group">
                    <label for="place">ตำบล</label>
                    <select  class="form-control" name="place" required >
                        <option value="{{Config::get('app.subdist')}}">{{Config::get('app.subdistname')}}</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="from-group">
                    <label for="district">อำเภอ</label>
                    <select  class="form-control" name="district" required >
                        <option value="{{Config::get('app.district')}}">{{Config::get('app.districtname')}}</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="from-group">
                    <label for="province">จังหวัด</label>
                      <select  class="form-control" name="province" required >
                          <option value="{{Config::get('app.province')}}">{{Config::get('app.provincename')}}</option>
                      </select>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="from-group">
                    <label for="postnum">รหัสไปรษณีย์</label>
                    <input type="text" class="form-control" name="postnum" placeholder="รหัสไปรษณีย์" value="{{$patients->PostalCode}}" required readonly>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="from-group">
                  <label for="phone">โทรศัพท์</label>
                  <input type="text" class="form-control" name="phone" placeholder="โทรศัพท์" value="{{$patients->Telephone}}" required>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>


        <!-- panel 2  -->
        <div class="col-md-6">
          <div class="panel panel-default">
            <!-- Default panel2 contents -->
            <div class="panel-heading">
              <h5>เพิ่มประวัติส่วนตัวผู้สูงอายุ (ต่อ)</h5>
            </div>
            <!-- Table2 -->
            <div class="panel-body">
              <div class="form-group">
                <label for="Sickhistory">ประวัติการเจ็บป่วยในอดีต</label>
                <input type="text" class="form-control" name="Sickhistory" id="tags_1" placeholder="ประวัติการเจ็บป่วยในอดีต" value="{{$patientsHistory->Pastillness}}">
              </div>

              <div class="form-group">
                <label for="Surgeryhistory">ประวัติการผ่าตัด</label>
                <input type="text" class="form-control" name="Surgeryhistory"  id="tags_2" placeholder="ประวัติการผ่าตัด" value="{{$patientsHistory->Historysurgery}}">
              </div>

              <div class="form-group">
                <label for="congenital">โรคประจำตัว</label>
                <input type="text" class="form-control" name="congenital"  id="tags_3" placeholder="โรคประจำตัว" value="{{$patientsHistory->Congenital}}">
              </div>

              <div class="form-group">
                <label for="Drug">ยาที่รับประทานประจำ</label>
                <input type="text" class="form-control" name="Drug"  id="tags_4" placeholder="ยาที่รับประทานประจำ" value="{{$patientsHistory->Drugidentification}}">
              </div>

              <div class="form-group">
                <label for="Allergy">ประวัติการแพ้ยาหรืออาหาร</label>
                <input type="text" class="form-control" name="Allergy"  id="tags_5" placeholder="ประวัติการแพ้ยาหรืออาหาร" value="{{$patientsHistory->Allergic}}">
              </div>

              <div class="from-group">
                <label for="cigarette">การสูบบุหรี่/ยาเส้น</label>
                <div class="row">
                  <div class="radio" >
                    <div class="col-md-7">
                      <label class="radio-inline">
                        <input type="radio" name="cigarette" value="1"  checked  onclick="disable_cigarette()" {{$patientsHistory->cig1}}> ไม่สูบ
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="cigarette" value="2" onclick="disable_cigarette()" {{$patientsHistory->cig2}}> สูบบ้าง
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="cigarette" value="3" onclick="disable_cigarette()" {{$patientsHistory->cig3}}> สูบประจำ
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="cigarette" value="4" onclick="enable_cigarette()" {{$patientsHistory->cig4}}> เคยสูบ
                      </label>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <input type="text" class="form-control input-sm" id="cigarette_enable" name="yearUseto" placeholder="เลิกสูบ...ปี"
                    <?php echo $disabled; ?> value="{{$patientsHistory->CigaretteTime}}">
                  </div>
                </div>
              </div>

              <div class="from-group" style="padding-top:8px">
                  <label for="alcohol">การดื่มเครื่องดื่มที่มีส่วนผสมของแอลกอฮอล์(เช่น สุรา เบียร์ ยาดองเหล้า)</label>
                  <div class="row">
                    <div class="radio" >
                      <div class="col-md-7">
                        <label class="radio-inline">
                          <input type="radio" name="alcohol" value="1" checked onclick="disable_alcohol()" {{$patientsHistory->dr1}}> ไม่ดื่ม
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="alcohol" value="2" onclick="disable_alcohol()" {{$patientsHistory->dr2}}> ดื่มบ้าง
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="alcohol" value="3" onclick="disable_alcohol()" {{$patientsHistory->dr3}}> ดื่มประจำ
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="alcohol" value="4" onclick="enable_alcohol()" {{$patientsHistory->dr4}}> เคยดื่ม
                        </label>
                      </div>
                  </div>
                  <div class="col-sm-3">
                    <input type="text" class="form-control input-sm" id="alcohol_enable" name="alcoholuseto" placeholder="เลิกดื่ม...ปี"
                    <?php echo $disabled; ?> value="{{$patientsHistory->Drinktime}}">
                  </div>
                </div>
              </div>

              <div class="form-group" style="padding-top:15px">
                <div class="row">
                  <div class="col-md-3 control-label">
                    <label for="submitday" style="padding-top:5px;">บันทึกประวัติครั้งที่</label>
                  </div>
                  <div class="col-md-5">
                    <div class="form-group">
                      <select  class="form-control" id="time" name="time">
                        @for ($i=1; $i <= $maxTime; $i++)
                          <option value="{{$i}}"  {{($patientsHistory->Time==$i)?'selected':''}} >{{$i}} (มีข้อมูลเเล้ว)</option>
                        @endfor
                        <option value="{{$i}}" {{($patientsHistory->Time==$i)?'selected':''}}>{{$i}}</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-md-3 control-label">
                    <label for="submitday" style="padding-top:5px;">วันที่บันทึกประวัติ</label>
                  </div>
                  <div class="col-md-5">
                    <div class="form-group">
                      <input type="text" class="form-control" name="submitday" data-inputmask="'mask': '99-99-9999'" placeholder="dd-mm-yyyy"  readonly="readonly" value="{{($patientsHistory->Time-1==$maxTime)?'':$patientsHistory->datesubmit()}}" required>
                    </div>
                  </div>
                </div>
              </div>

              <div class="text-center">
                <button type="submit" class="btn btn-success">บันทึก</button>
              </div>

              <!-- end Table2 -->
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>

@stop

@section('js')
  <script src="{{ URL::asset('theme/dist/jquery.inputmask.bundle.js'); }}"></script>
  <script src="{{ URL::asset('theme/tagsInput/src/jquery.tagsinput.js'); }}"></script>
  <script src="{{ URL::asset('theme/bootstrap-datepicker-thai/js/bootstrap-datepicker-thai.js'); }}"></script>
  <script type="text/javascript">
    $('#tags_1').tagsInput({width:'auto'});
    $('#tags_2').tagsInput({width:'auto'});
    $('#tags_3').tagsInput({width:'auto'});
    $('#tags_4').tagsInput({width:'auto'});
    $('#tags_5').tagsInput({width:'auto'});


    $(".datepicker").datepicker({
       format: "yyyy/mm/dd",
       language:'th-th',
       autoclose: true,
    });


    function enable_alcohol(){
      $('#alcohol_enable').attr('disabled', false);
    }
    function disable_alcohol(){
      $('#alcohol_enable').attr('disabled', true);
    }
    function enable_cigarette(){
      $('#cigarette_enable').attr('disabled', false);
    }
    function disable_cigarette(){
      $('#cigarette_enable').attr('disabled', true);
    }

    $( "#time" ).change(function() {
        window.location.replace("{{Request::url()}}?time="+$(this).val());
    });

    $( ".prefix" )
    .change(function() {
      var val = $(this).find("option:selected").val();
      if (val==="นาย") {
        $(".gender").val("1");
      }
      else if (val==="นาง" || val==="นางสาว") {
        $(".gender").val("0");
      }
      else {
        $(".gender").val("");
      }

    })
    .trigger( "change" );

    $('input[name=submitday]').datepicker_thai({
		dateFormat: 'dd-mm-yy',
        changeMonth: false,
        changeYear: true,		
		numberOfMonths: 1,
		langTh:true,
		yearTh:true,
	});

  $('input[name=birthday]').datepicker_thai({
		dateFormat: 'dd-mm-yy',
    changeMonth: false,
    changeYear: true,		
		numberOfMonths: 1,
		langTh:true,
		yearTh:true
	});
</script>
@stop
