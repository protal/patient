
@extends('layouts.master')
@section('title')
  สรุปบันทึกสุขภาพ
@stop

@section('body')
  <div class="container">
     ค้นหา
        <div style="margin-bottom:20px;">
            <form class="" action="" method="get">

                <select class="btn dropdown-toggle btn-primary   selectvillage" title="เลือกหมู่บ้าน" name="village">
                    @if (isset($village))
                        <option value="">
                            หมู่บ้านทั้งหมด
                        </option>
                        @foreach ($village as $v)
                            <option value="{{$v}}" {{($v==$q_village)?"selected":""}}>
                                หมู่ที่ {{$v}}
                            </option>
                        @endforeach
                    @endif
                </select>

                <select class="btn dropdown-toggle btn-primary  volunteer" name="volunteer" {{($q_village=='')?'disabled':''}}>
                    <option value=''>อสม. ทั้งหมด</option>
                    @foreach ($vola as $v)
                        <option value="{{$v->pid}}" {{($v->pid==$q_volunteer)?"selected":""}}>{{$v->fname}} {{$v->lname}}</option>
                    @endforeach
                </select>

                <select class="btn dropdown-toggle btn-primary" name="years" id="years">
                    @foreach ($years as $year)
                        <option value="{{$year->record_years}}" {{($year->record_years==$q_years)?"selected":""}}>พ.ศ. {{$year->record_years}}</option>
                    @endforeach
                </select>

                <select class="btn dropdown-toggle btn-primary" name="times" id="times">
                    @foreach ($recordTimes as $record )
                        <option value="{{$record->record_times}}"  {{($record->record_times==$q_times)?"selected":""}}>
                            ครั้งที่ {{$record->record_times}}
                        </option>
                    @endforeach
                </select>
                <button type="submit" class="btn btn-primary">ค้นหา</button>
            </form>
        </div>
    @if (!isset($error))
      <div id="chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    @else
      ไม่พบข้อมูล
    @endif

  </div>
@stop
@section('css')
    {{--  bootstrap-select  --}}
    <link rel="stylesheet" href="{{ URL::asset('theme/bootstrap-select/dist/css/bootstrap-select.css') }}">

@stop

@section('js')
    <script src="{{ URL::asset('theme/bootstrap-select/dist/js/bootstrap-select.js')}}"></script>
    <script src="{{ URL::asset('theme/highcharts/highcharts.js'); }}"></script>
    <script src="{{ URL::asset('theme/highcharts/modules/data.js'); }}"></script>
    <script src="{{ URL::asset('theme/highcharts/modules/drilldown.js'); }}"></script>
    <link href="{{ URL::asset('theme/highcharts/css/highcharts.css'); }}" rel="stylesheet">
    <script type="text/javascript">
          $('#years').change(function(event) {
            var year = $(this);
            var time = $("#times");

            $.get( "{{url('api/recordtime/year/')}}/"+year.val(), function( data ) {

                time.html("");
                $.each(data, function(key,value) {
                    console.log(value);
                    time.append($("<option></option>").attr("value",value).text("ครั้งที่ "+value));
                });

            });
        });

        $('.selectvillage').change(function(event) {
            var village = $(this);
            var volunteer = $(".volunteer");
            var url = "{{url('api/villtovol')}}/"+village.val();
            console.log(url);
            console.log(village);
            if(village.val()=='')
                volunteer.prop('disabled', true);
            else
                volunteer.prop('disabled', false);
            $.get( url, function( data ) {
                volunteer.html("<option value=''>อสม. ทั้งหมด</option>");
                $.each(data, function(key,value) {
                    console.log(value);
                    volunteer.append($("<option></option>").attr("value",value['pid']).text(value['fname']+" "+value["lname"]));
                });

            });
        });
    </script>
    @if (!isset($error))
        <script type="text/javascript">
        // Create the chart
        Highcharts.chart('chart', {
            chart: {
                type: 'column'
            },
            title: {
                text: '{{$part->name}}'
            },
            subtitle: {
                text: 'คลิกที่คอลัมน์เพื่อดูเพศชายเพศหญิง'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'จำนวนผู้ป่วย'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    events:{
                        click: function (event) {
                        //
                        if(!event.point.drilldown)
                        {
                            var url = "{{url('graph/'.$part->id.'/'.$result_id)}}"
                            url = url+'/'+event.point.series.userOptions.id;
                            if(event.point.name === "เพศชาย")
                            {
                            url = url+'/'+'1';
                            }
                            else
                            {
                            url = url+'/'+'0';
                            }
                            <?php
                            if(isset($search_output))
                            {
                                echo "url = url+'?".$search_output."';";
                            }
                            ?>
                            var w = 700;
                            var h = 400;
                            var left = (screen.width/2)-(w/2);
                            var top = (screen.height/2)-(h/2);
                            newwindow=window.open(url,"test",'height='+h+',width='+w+',top='+top+', left='+left);
                            if (window.focus) {newwindow.focus()}
                        }

                        }
                    },
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> คนของผู้ป่วยทั้งหมด<br/>'
            },

            series: [{
                name: ' ',
                colorByPoint: true,
                data: {{$series}},
            }],
            drilldown:{
            series: {{$d_series}}
            }
            });





        </script>

    @endif


@stop
