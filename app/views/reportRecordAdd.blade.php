@extends('layouts.master')
@section('title')
  ลงบันทึก
@stop

@section('body')
<form class="form-group" method="post" data-parsley-validate novalidate>
<div class="panel panel-default">

  <!-- Default panel contents -->

    <div class="panel-heading">
      <h5>{{$form->name}} ( ปี {{$years}} ครั้งที่ {{$time}})</h5>
    </div>

  <!-- Table -->

    <table class="table ">
      <thead>
        <tr>
          <td>#</td>
          <td >หัวข้อ</td>
          <td>ตัวเลือก</td>
        </tr>
      </thead>
      <tbody>
        <?php $i=0; ?>
        <?php foreach ($form->topic->all() as $topic): ?>
          <tr>
            <td>{{++$i}}</td>
            <td style="max-width:250px"><p>{{$topic->name}}</p></td>
            <td>
              <?php $last = -1; ?>
              <?php foreach ($topic->scores as $score): ?>
                <?php if ($last != $topic->id):
                        $re = 'checked';
                      else:
                        $re = "";
                      endif; ?>
                <div class="radio">
                  <input type="radio" name="{{$topic->id}}" value="{{$score->score}}" {{$re}}> <span class="badge badge-success">{{$score->score}} คะแนน</span> {{$score->name}}
                </div>

               <?php $last = $topic->id; ?>
              <?php endforeach; ?>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <div class="panel-body">
      <div class="text-right">
        <button type="submit" class="btn btn-default">บันทึก</button>
      </div>
    </div>



</div>
</form>

@stop

@section('js')
<script type="text/javascript">


$(".datepicker").datepicker({
   format: "dd/mm/yyyy",
   language:'th-th',
   autoclose: true,
});
</script>
@stop
