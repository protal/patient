@extends('layouts.master')
@section('title')
  สรุปบันทึกสุขภาพ
@stop

@section('body')
  <div class="container">
    
      ค้นหา
        <div style="margin-bottom:20px;">
            <form class="" action="" method="get">

                <select class="btn dropdown-toggle btn-primary selectvillage" title="เลือกหมู่บ้าน" name="village">
                        @foreach ($villages as $v)
                            <option value="{{$v->village}}" {{($v->village==$village)?"selected":""}}>
                                	หมู่ที่ {{$v->village}}
                            </option>
                        @endforeach
                </select>

                <select class="btn dropdown-toggle btn-primary" name="year" id="year">
                    <?php foreach ($recordYear as $y){ ?>
                        <option value="{{$y->record_years}}" {{($y->record_years==$year)?"selected":""}}>พศ {{$y->record_years}}</option>
                    <?php } ?>
                </select>

                <button type="submit" class="btn btn-primary">ค้นหา</button>
            </form>
        </div>
   	  @if($error == 0)
      <div id="chart" style="min-width: 310px; height: 400px; margin: 0 auto">

      </div>
    @else
      ไม่พบข้อมูล
    @endif

  </div>
@stop
@section('css')
    {{--  bootstrap-select  --}}
    <link rel="stylesheet" href="{{ URL::asset('theme/bootstrap-select/dist/css/bootstrap-select.css') }}">

@stop

@section('js')
    <script src="{{ URL::asset('theme/bootstrap-select/dist/js/bootstrap-select.js')}}"></script>
    <script src="{{ URL::asset('theme/highcharts/highcharts.js'); }}"></script>
<!--     <script src="{{ URL::asset('theme/highcharts/modules/data.js'); }}"></script> -->
<!--     <script src="{{ URL::asset('theme/highcharts/modules/drilldown.js'); }}"></script> -->
    <link href="{{ URL::asset('theme/highcharts/css/highcharts.css'); }}" rel="stylesheet">
    @if ($error == 0)
        <script type="text/javascript">
        // Create the chart
		
        Highcharts.chart('chart', {
            chart: {
                type: 'column'
            },
            title: {
                text: '{{$part->name}}'
            },
            subtitle: {
                text: 'คลิกที่คอลัมน์เพื่อดูเพศชายเพศหญิง'
            },
            xAxis: {
                categories: {{$arrResult}},
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'จำนวนผู้ป่วย (คน)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f} คน</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                },
                series: {
                    events:{
                        click: function (event) {
							var data = event.point.series.userOptions;
							var selectDesc = event.point.category;
							var param = "/"+data.formId+"/"+data.village+"/"+data.year+"/"+data.time+"/"+selectDesc;
							var url = '{{url('conclude')}}'+param
							var w = 700;
	                        var h = 400;
	                        var left = (screen.width/2)-(w/2);
	                        var top = (screen.height/2)-(h/2);
	                            newwindow=window.open(url,"test",'height='+h+',width='+w+',top='+top+', left='+left);
	                        if (window.focus) {newwindow.focus()}
						
                        }
                    }
                }
            },
            series: {{$result}}
        });
        </script>

    @endif


@stop
