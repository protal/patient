<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>ผลสรุป - {{Config::get('app.title')}}</title>
    <!-- Bootstrap -->

    <link href="{{ URL::asset('theme/bootstrap/dist/css/bootstrap.min.css'); }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ URL::asset('theme/font-awesome/css/font-awesome.min.css'); }}" rel="stylesheet">





  </head>
  <body  class="nav-md">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>ชื่อ</th>
          <th>นามสกุล</th>
          <th>อายุ</th>
          <th>อสม.</th>
          <th>ที่อยู่</th>
        </tr>
      </thead>
      <tbody>
        <?php $i = 0 ; ?>
        @foreach ($patient_result as $p)
          <tr>
            <th scope="row">{{++$i}}</th>
            <td>{{$p->prefix}} {{$p->Firstname}}</td>
            <td>{{$p->Sirname}}</td>
            <td>{{$p->age}}</td>
            <td>{{$p->volunteer}}</td>
            <td>
              @if (isset($p->HomeNo))
                บ้านเลขที่ {{$p->HomeNo}}
              @endif
              @if (isset($p->Village))
                หมู่ที่ {{$p->Village}}
              @endif
              @if (isset($p->Alley))
                ซอย {{$p->Alley}}
              @endif
              @if (isset($p->Road))
                ถนน {{$p->Road}}
              @endif

            </td>
          </tr>

        @endforeach
      </tbody>
    </table>


    <!-- jQuery -->
    <script src="{{ URL::asset('theme/jquery/dist/jquery.min.js'); }}"></script>
    <!-- Bootstrap -->
    <script src="{{ URL::asset('theme/bootstrap/dist/js/bootstrap.min.js'); }}"></script>


  </body>
</html>
