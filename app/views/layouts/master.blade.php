<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title') - {{Config::get('app.title')}}</title>
    <!-- Bootstrap -->

    <link href="{{ URL::asset('theme/bootstrap/dist/css/bootstrap.min.css'); }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ URL::asset('theme/font-awesome/css/font-awesome.min.css'); }}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ URL::asset('theme/nprogress/nprogress.css'); }}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{ URL::asset('theme/bootstrap-daterangepicker/daterangepicker.css'); }}" rel="stylesheet">
    <!-- Ion.RangeSlider -->
    <link href="{{ URL::asset('theme/normalize-css/normalize.css'); }}" rel="stylesheet">
    <link href="{{ URL::asset('theme/ion.rangeSlider/css/ion.rangeSlider.css'); }}" rel="stylesheet">
    <link href="{{ URL::asset('theme/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css'); }}" rel="stylesheet">
    <!-- Bootstrap Colorpicker -->
    <link href="{{ URL::asset('theme/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css'); }}" rel="stylesheet">

    <link href="{{ URL::asset('theme/cropper/dist/cropper.min.css'); }}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ URL::asset('theme/build/css/custom.min.css'); }}" rel="stylesheet">

    <link href="{{ URL::asset('theme/jquery-ui/jquery-ui.css'); }}" rel="stylesheet">


    @yield('css')


  </head>
  <body  class="nav-md">

    <div class="container body">
          <div class="main_container">
            <div class="col-md-3 left_col">
              <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                  <a href="{{url('/')}}" class="site_title"><i class="fa fa-plus"></i> <span>บันทึกสุขภาพ</span></a>
                </div>

                <div class="clearfix"></div>


                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                  <div class="menu_section">
                    <h3>ทั่วไป</h3>
                    <ul class="nav side-menu">
                      {{--  <li><a href="{{{url('/')}}}"><i class="fa fa-home"></i> หน้าแรก </a></li>  --}}
                      <li><a href="{{{url('patient')}}}"><i class="fa fa-edit"></i> บันทึกสุขภาพ </a></li>
                      <li><a href="{{{url('graph')}}}"><i class="fa fa-bar-chart"></i> กราฟสรุปบันทึกสุขภาพ </a></li>
                      <li><a href="{{{url('conclude')}}}"><i class="fa fa-line-chart"></i> สรุปสุขภาพผู้สูงอายุ </a></li>
					  <li><a href="{{{url('health')}}}"><i class="fa fa-map"></i> แสดงผลบนแผนที่ </a></li>
                    </ul>
                  </div>
                  @if(Auth::user()->isAdmin())
                  <div class="menu_section">
                    <h3>จัดการระบบ</h3>
                    <ul class="nav side-menu">
                      <li><a href="{{{url('admin/report')}}}"><i class="fa fa-windows"></i> จัดการบันทึก </a></li>
                      <li><a href="{{{url('admin/recordTime')}}}"><i class="fa fa-gitlab"></i> จัดการครั้งที่บันทึก </a></li>
                      <li><a href="{{{url('admin/village')}}}"><i class="fa fa-building"></i> จัดการหมู่บ้าน  </a></li>
                      <li><a href="{{{url('admin/person')}}}"><i class="fa fa-users"></i> จัดการ อสม. </a></li>
                      <li><a href="{{{url('admin/user')}}}"><i class="fa fa-sitemap"></i> จัดการสมาชิก </a></li>
                    </ul>
                  </div>
                  @endif

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                  <a data-toggle="tooltip" data-placement="top" title="Settings">
                    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                  </a>
                  <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                    <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                  </a>
                  <a data-toggle="tooltip" data-placement="top" title="Lock">
                    <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                  </a>
                  <a href="{{url('/logout')}}" data-toggle="tooltip" data-placement="top" title="Logout" >
                    <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                  </a>
                </div>
                <!-- /menu footer buttons -->
              </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">
              <div class="nav_menu">
                <nav>
                  <div class="nav toggle">
                    <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                  </div>

                  <ul class="nav navbar-nav navbar-right">
                    <li class="">
                      <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        {{Auth::user()->firstname}} {{Auth::user()->lastname}}
                        <span class=" fa fa-angle-down"></span>
                      </a>
                      <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <!-- <li><a href="javascript:;"> Profile</a></li>
                        <li>
                          <a href="javascript:;">
                            <span class="badge bg-red pull-right">50%</span>
                            <span>Settings</span>
                          </a>
                        </li>
                        <li><a href="javascript:;">Help</a></li> -->
                        <li><a href="{{url('/logout')}}"><i class="fa fa-sign-out pull-right"></i> ออกจากระบบ</a></li>
                      </ul>
                    </li>


                  </ul>
                </nav>
              </div>
            </div>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <div>
                  @section('breadcrumbs')
                    @if (Route::currentRouteName())
                      {{Breadcrumbs::render()}}
                    @endif
                  @show
                </div>
                <div class="page-title">
                  <div class="title_left">
                    <h3>@yield('title')</h3>
                  </div>
                </div>

                <div class="clearfix"></div>
                <div class="container">
                  @include('error')
                  @yield('body')
                </div>
            </div>
            <!-- /page content -->

            <!-- footer content -->
            <footer>
              <div class="pull-right">
                WU.AC.TH
              </div>
              <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
          </div>
        </div>


    <!-- jQuery -->
    <script src="{{ URL::asset('theme/jquery/dist/jquery.min.js'); }}"></script>
    <script src="{{ URL::asset('theme/jquery-ui/jquery-ui.js'); }}"></script>
    <!-- Bootstrap -->
    <script src="{{ URL::asset('theme/bootstrap/dist/js/bootstrap.min.js'); }}"></script>
    <!-- FastClick -->
    <script src="{{ URL::asset('theme/fastclick/lib/fastclick.js'); }}"></script>
    <!-- NProgress -->
    <script src="{{ URL::asset('theme/nprogress/nprogress.js'); }}"></script>
    <!-- validator -->
    <script src="{{ URL::asset('theme/parsleyjs/dist/parsley.min.js'); }}"></script>
    <script src="{{ URL::asset('theme/parsleyjs/dist/i18n/th.js'); }}"></script>
    <!-- Custom Theme Scripts -->
    <script src="{{ URL::asset('theme/build/js/custom.min.js'); }}"></script>



    <script type="text/javascript">
      function show_confirm(obj){
        var r=confirm("คุณต้องการที่จะลบใช่หรือไม่?");
        if (r==true)
           window.location = obj.attr('href');
      }
      $(function(){
        $('.delete').click(function(event) {
          event.preventDefault();
          show_confirm($(this));
        });
      });

    </script>


    @yield('js')
  </body>
</html>
