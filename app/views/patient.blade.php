@extends('layouts.master')
@section('title')
  บันทึกสุขภาพ
@stop

@section('body')

<style media="screen">
#Special {
width: 200px;
float: right;
}

.textColor{
  color: #000 !important ;
}
</style>
<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">
    <h5 class="pull-left">รายชื่อผู้เข้ารับบริการ</h5>

      <form class="input-group" action="" method="get">

        <input id="Special" name="q" type="text" class="form-control" value="{{(isset($q)?$q:"")}}" placeholder="ค้นหา">

        <select id="Special" name="type" class="btn dropdown-toggle btn-defult">
            <option value="name">ค้นหาโดยชื่อหรือนามสกุล</option>
            <option value="volunteer" <?=(isset($type)&&$type=="volunteer")?'selected':''?>>ค้นหาโดยอสม.</option>
        </select>

        <div class="input-group-btn">
          <button class="btn btn-primary"><i class="glyphicon glyphicon-search"></i></button>
          <a href="{{{url('patient/add')}}}" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เพิ่ม</a>
        </div>
      </form>


  </div>

  @if ($patients->count() > 0)
    <!-- Table -->
    <table class="table ">
      <thead>
        <tr>
          <td>#</td>
          <td>ชื่อ</td>
          <td>นามสกุล</td>
          <td>อายุ</td>
          <td>อสม.</td>
          <td width="30%">ดำเนินการ</td>
        </tr>
      </thead>
      <tbody>
        <?php $count = ($patients->getCurrentPage()-1) * $patients->getPerPage()  ; ?>
        <?php foreach($patients as $patient): ?>
        <tr>
          <td>{{++$count}}</td>
          <td>{{$patient->Firstname}}</td>
          <td>{{$patient->Sirname}}</td>
          <td>{{$patient->age()}}</td>
          @if ($v = $patient->volunteer())
            <td>{{$v->fname}} {{$v->lname}}</td>
          @else
            <td></td>
          @endif

          <td>
            <div class="dropdown">
              <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                จัดการข้อมูล
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li>
                  <a href="{{{url('patient/'.$patient->id.'/report')}}}" class="btn btn-success btn-sm textColor">
                  <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> กรอกแบบฟอร์ม</a>
                </li>
                <li>
                  <a href="{{{url('patient/'.$patient->id.'/edit/')}}}" class="btn btn-primary  btn-sm textColor">
                  <span class="glyphicon glyphicon-edit" aria-hidden="true"></span> แก้ไขข้อมูล</a>
                </li>
                <li>
                  <a href="{{{url('patient/'.$patient->id.'/delete/')}}}" class="btn btn-danger  btn-sm delete textColor">
                  <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> ลบ</a>
                </li>
              </ul>
            </div>
          </td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <div class="panel-body text-center">
        <?php echo $patients->appends(array('q' => $q))->appends(array('type' => $type))->links(); ?>
    </div>
  @else
    <div class="panel-body text-center">
      ...ไม่พบข้อมูล...
    </div>
  @endif
</div>

@stop
