@extends('layouts.master')
@section('title')
  ลงบันทึกสุขภาพ
@stop

@section('js')
  <script type="text/javascript">
    $('#years').change(function(event) {
     var year = $(this);
     var time = $("#times");
     $.get( "{{url('api/recordtime/year')}}/"+year.val(), function( data ) {

        time.html("");
        time.append($("<option></option>").attr("value","").text("ครั้งทั้งหมด"));
        $.each(data, function(key,value) {
          console.log(value);
          time.append($("<option></option>").attr("value",value).text("ครั้งที่ "+value));
       });

     });
   });
  </script>
@endsection

@section('body')
  <style media="screen">
  #Special {
  width: 200px;
  float: right;
  }

  .textColor{
    color: #000 !important ;
  }
  </style>
<div class="panel panel-default">
  <div class="panel-heading">

    <form class="pull-right" action="" method="get">
      <select class="btn dropdown-toggle btn-defult" name="years" id="years">
        <option value="">ปีทั้งหมด</option>
        @foreach ($years as $y)
          <option value="{{$y->record_years}}" {{($y->record_years==$io_years)?'selected':''}}>{{$y->record_years}}</option>
        @endforeach
      </select>

      <select class="btn dropdown-toggle btn-defult" name="times" id="times">
        <option value="">ครั้งทั้งหมด</option>
        @foreach ($times as $t)
          <option value="{{$t->record_times}}" {{($t->record_times==$io_times)?'selected':''}}>{{$t->record_times}}</option>
        @endforeach
      </select>
      <button type="submit" class="btn btn-primary">ค้นหา</button>
    </form>
    <h5>{{$form->name}}</h5>
  </div>
  @if($time->count()!=0)
  <table class="table table-striped">

    <thead>
      <tr>
        <td>ปีที่</td>
        <td>ครั้งที่</td>
        <td>ผลการประเมิน</td>
        <td>กำเนินการ</td>
      </tr>
    </thead>
    <tbody>
      <?php $count = ($time->getCurrentPage()-1) * $time->getPerPage()  ; ?>
      @foreach($time as $t)
      <?php $r = $t->result($id,$form_id);  ?>
      <tr>
        <td>{{$t->record_years}}</td>
        <td width="30%">{{$t->record_times}}</td>
        @if (isset($r))
          <td width="40%">{{$r->result_name}}</td>
          <td >
            <a href="{{{url('/patient/'.$id.'/record/'.$form_id.'/result/'.$r->id.'/edit')}}}" class="btn btn-primary  btn-sm"> <span class="glyphicon glyphicon-edit" aria-hidden="true"> แก้ไข</span></a>
            <a href="{{{url('/patient/'.$id.'/record/'.$form_id.'/result/'.$r->id.'/delete')}}}" class="btn btn-danger  btn-sm delete"> <span class="glyphicon glyphicon-remove" aria-hidden="true"> ลบ</span></a>
          </td>
        @else
          <td width="40%"></td>
          <td >
            <a href="{{{url('/patient/'.$id.'/record/'.$form_id.'/add/'.$t->record_years.'/'.$t->record_times)}}}" class="btn btn-primary">เพิ่ม</a>
          </td>
        @endif

      </tr>
      @endforeach
    </tbody>
  </table>
  <div class="panel-body text-center">
      <?php echo $time->links(); ?>
  </div>
  @else
  <div class="panel-body text-center">
    ...ไม่พบครั้งที่บันทึก กรุณา <a href="{{url('admin/recordTime')}}">เพิ่มครั้งที่บันทึก</a>...
  </div>
  @endif
</div>


@stop
