<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>เข้าสู่ระบบ - {{Config::get('app.title')}}</title>
    <!-- Bootstrap -->

    <link href="{{ URL::asset('theme/bootstrap/dist/css/bootstrap.min.css'); }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ URL::asset('theme/font-awesome/css/font-awesome.min.css'); }}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ URL::asset('theme/nprogress/nprogress.css'); }}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{ URL::asset('theme/bootstrap-daterangepicker/daterangepicker.css'); }}" rel="stylesheet">
    <!-- Ion.RangeSlider -->
    <link href="{{ URL::asset('theme/normalize-css/normalize.css'); }}" rel="stylesheet">
    <link href="{{ URL::asset('theme/ion.rangeSlider/css/ion.rangeSlider.css'); }}" rel="stylesheet">
    <link href="{{ URL::asset('theme/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css'); }}" rel="stylesheet">
    <!-- Bootstrap Colorpicker -->
    <link href="{{ URL::asset('theme/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css'); }}" rel="stylesheet">

    <link href="{{ URL::asset('theme/cropper/dist/cropper.min.css'); }}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ URL::asset('theme/build/css/custom.min.css'); }}" rel="stylesheet">



  </head>
  <body  class="login">

    <div class="login_wrapper">
      <div class="animate form login_form">


        <section class="login_content">

          <form action="" method="post" data-parsley-validate class="form-group form-horizontal form-label-left" novalidate>
            <h1><i class="fa fa-plus"></i> บันทึกสุขภาพ </h1>
            <p>{{Config::get('app.hph')}}</p>
            @include('error')
            <div>
              <input type="text" class="form-control" name="username" placeholder="ชื่อผู้ใช้" required="required">
            </div>
            <div>
              <input type="password" class="form-control" name="password" placeholder="รหัสผ่าน" required="required">
            </div>
            <div>
              <input type="checkbox" name="remember" value="1" class="flat" /> จดจำรหัสผ่าน <br><br>
            </div>
            <div>
              <button type="submite" class="btn btn-default" name="button">เข้าสู่ระบบ</button>
            </div>

              <div Style="margin-top:30px">
                <p>©2017 Wu.ac.th</p>
              </div>
          </form>
        </section>
      </div>
    </div>
    <!-- jQuery -->
    <script src="{{ URL::asset('theme/jquery/dist/jquery.min.js'); }}"></script>
    <!-- Bootstrap -->
    <script src="{{ URL::asset('theme/bootstrap/dist/js/bootstrap.min.js'); }}"></script>
    <!-- FastClick -->
    <script src="{{ URL::asset('theme/fastclick/lib/fastclick.js'); }}"></script>
    <!-- NProgress -->
    <script src="{{ URL::asset('theme/nprogress/nprogress.js'); }}"></script>
    <!-- validator -->
    <script src="{{ URL::asset('theme/parsleyjs/dist/parsley.min.js'); }}"></script>
    <script src="{{ URL::asset('theme/parsleyjs/dist/i18n/th.js'); }}"></script>
    <!-- Custom Theme Scripts -->
    <script src="{{ URL::asset('theme/build/js/custom.min.js'); }}"></script>

    @yield('js')
  </body>
</html>
