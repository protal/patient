@extends('layouts.master')
@section('title')
  เพิ่มสมาชิก
@stop

@section('body')
  <div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">
      <h5>เพิ่มสมาชิก</h5>
    </div>

    <div class="panel-body">
      <form action="" method="post" data-parsley-validate class="form-horizontal form-label-left" novalidate>
        <div class="row">
          <div class="col-md-6">
            <div class="item form-group">
              <label for="firstname">ชื่อสมาชิก</label>
              <input type="text" class="form-control" name="firstname" placeholder="ชื่อสมาชิก" required="required" value="{{Input::old('firstname')}}">
            </div>
          </div>
          <div class="col-md-6">
            <fieldset class="form-group">
              <label for="lastname">นามสกุลสมาชิก</label>
              <input type="text" class="form-control" name="lastname" placeholder="นามสกุลสมาชิก" required="required" value="{{Input::old('lastname')}}">
            </fieldset>
          </div>
        </div>

        <fieldset class="form-group">
          <label for="username">ชื่อผู้ใช้</label>
          <input type="text" class="form-control" name="username" placeholder="ชื่อผู้ใช้" required="required" data-parsley-pattern="/^[a-zA-Z0-9]+$/" value="{{Input::old('username')}}">
        </fieldset>

        <div class="row">
          <div class="col-md-6">
            <fieldset class="form-group">
              <label for="password">รหัสผ่าน</label>
              <input type="password" id="password" class="form-control" name="password" placeholder="รหัสผ่าน" required="required">
            </fieldset>
          </div>
          <div class="col-md-6">
            <fieldset class="form-group">
              <label for="repassword">กรอกรหัสผ่านอีกครั้ง</label>
              <input type="password" class="form-control" name="repassword" placeholder="กรอกรหัสผ่านอีกครั้ง" required="required" data-parsley-equalto="#password">
            </fieldset>
          </div>
        </div>


        <fieldset class="form-group">
          <label for="exampleSelect1">สถานะ</label>
          <select class="form-control" name="type">
            <option value="0">ผู้จัดการระบบ</option>
            <option value="1">ผู้บันทึก</option>
          </select>
        </fieldset>

        <div class="text-right">
          <button type="submit" class="btn btn-primary">บันทึก</button>
        </div>

      </form>

    </div>
  </div>
@stop

