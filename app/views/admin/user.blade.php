@extends('layouts.master')
@section('title')
  จัดการสมาชิก
@stop

@section('body')
<style media="screen">
#Special {
width: 200px;
float: right;
}
</style>
  <div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">
      <h5 class="pull-left">จัดการสมาชิก</h5>

        <form class="input-group" action="" method="get">
          <input id="Special" name="q" type="text" class="form-control" value="{{(isset($q)?$q:"")}}" placeholder="ค้นหาโดยชื่อหรือนามสกุล">
          <div class="input-group-btn">
            <button class="btn btn-primary"><i class="glyphicon glyphicon-search"></i></button>
            <a href="{{{url('admin/user/add')}}}" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เพิ่ม</a>
          </div>
        </form>

    </div>

    <!-- Table -->
    <table class="table ">
      <thead>
        <tr>
          <td>#</td>
          <td>username</td>
          <td>ชื่อ</td>
          <td>นามสกุล</td>
          <td>สถาณะ</td>
          <td width="20%">เวลาเข้าใช้งาน</td>
          <td width="20%">ดำเนินการ</td>
        </tr>
      </thead>
      <tbody>
        <?php $count = ($users->getCurrentPage()-1) * $users->getPerPage()  ; ?>
        <?php foreach ($users as $user): ?>
        <tr>
          <td>{{++$count}}</td>
          <td>{{$user->username}}</td>
          <td>{{$user->firstname}}</td>
          <td>{{$user->lastname}}</td>
          <td>{{$user->types->name}}</td>
          <td>{{$user->lastTimeLogin()}}</td>
          <td>
            <a href="{{{url('admin/user/edit/'.$user->id)}}}" class="btn btn-primary  btn-sm"> <span class="glyphicon glyphicon-edit" aria-hidden="true"> แก้ไข</span></a>
            <a href="#" class="btn btn-danger  btn-sm remove_levels" data-url="{{{url('admin/user/delete/'.$user->id)}}}"> <span class="glyphicon glyphicon-remove" aria-hidden="true"> ลบ</span></a>
          </td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <div class="panel-body text-center">
      <?php if(isset($q)): ?>
        <?php echo $users->appends(array('q' => $q))->links(); ?>
      <?php else: ?>
        <?php echo $users->links(); ?>
      <?php endif ?>
    </div>
  </div>


  <!-- delete dialog -->
  <div id="confirm" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-body">
          Are you sure?
        </div>
        <div class="modal-footer">
          <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete</button>
          <button type="button" data-dismiss="modal" class="btn">Cancel</button>
        </div>
      </div>
    </div>
  </div>
  <div  class="modal hide fade">

  </div>
@stop

@section('js')
<script type="text/javascript">
  $('.remove_levels').on('click', function(e) {
  e.preventDefault();
  var url = $(this).data('url');
  $('#confirm').modal({
      backdrop: 'static',
      keyboard: false
    })
    .one('click', '#delete', function(e) {
      window.location.replace(url);
    });
  });
</script>
@stop
