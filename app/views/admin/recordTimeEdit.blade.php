@extends('layouts.master')
@section('title')
  แก้ไขปีและครั้งที่ทำการบันทึก
@stop

@section('body')
  <div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">
      <h5>แก้ไขปีและครั้งที่ทำการบันทึก</h5>
    </div>

    <div class="panel-body">
      <form action="" method="post" data-parsley-validate class="form-horizontal form-label-left" novalidate>

        <fieldset class="form-group">
          <div class="row">
            <div class="col-md-6">
              <label for="ryear">ปีที่ทำการบันทึก</label>
              <input type="number" class="form-control" value="{{$recordTime->record_years}}" name="ryear" placeholder="ปีที่ทำการบันทึก"  required="required" >
            </div>
            <div class="col-md-6">
              <label for="rtime">ครั้งที่ทำการบันทึก</label>
              <input type="number" class="form-control" value="{{$recordTime->record_times}}" name="rtime" placeholder="ครั้งที่ทำการบันทึก"  required="required" >
            </div>
          </div>
        </fieldset>

        <div class="text-right">
          <button type="submit" class="btn btn-primary">บันทึก</button>
        </div>

      </form>
    </div>
  </div>
@stop
