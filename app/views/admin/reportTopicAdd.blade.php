@extends('layouts.master')
@section('title')
  เพิ่มหัวข้อ
@stop

@section('body')
  <div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">
      <h5>เพิ่มหัวข้อ</h5>
    </div>

    <div class="panel-body">
      <form action="" method="post" data-parsley-validate novalidate>

        <fieldset class="form-group">
          <label for="exampleInputEmail1">หัวข้อ</label>
          <input type="text" class="form-control" name="name" required="required" placeholder="หัวข้อบันทึก">
        </fieldset>
        <hr>

        <a class="add_field_button btn btn-info">เพิ่มคะแนน</a>

        <fieldset>
          <div class="wrap-input">
            <div class="row form-add">
              <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                  <label for="exampleInputEmail1">ชื่อคะแนน</label>
                  <input type="text" name="scorename[]" placeholder="ชื่อคะแนน" required="required" class="form-control">
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                  <label for="exampleInputEmail1">หน่วยคะแนน</label>
                  <input type="number" name="score[]" placeholder="หน่วยคะแนน" required="required" class="form-control">
              </div>
            </div>

          </div>


        </fieldset>
        <div class="text-right">
          <button type="submit"  class="btn btn-primary">บันทึก</button>
        </div>

      </form>
    </div>
  </div>
@stop

@section('js')

<script type="text/javascript">
  var wrapper         = $(".wrap-input");
  var add_button      = $(".add_field_button");
  var moredata      = $(".more-data");

  $(add_button).click(function(e){
    e.preventDefault();
    var newElement='<div class="row form-add"> <div class="col-md-6 col-sm-6 col-xs-6 form-group"> <label for="exampleInputEmail1">ชื่อคะแนน</label> <input type="text" name="scorename[]" placeholder="ชื่อคะแนน" class="form-control" required="required"> </div> <div class="col-md-5 col-sm-5 col-xs-5 form-group"> <label for="exampleInputEmail1">หน่วยคะแนน</label> <input type="number" name="score[]" placeholder="หน่วยคะแนน" class="form-control" required="required"> </div> <div class="col-md-1 col-sm-1 col-xs-1 form-group"> <br> <button href="#" class="btn btn-danger btn-sm remove_field"> <span class="glyphicon glyphicon-remove" aria-hidden="true"> ลบ</span></button> </div> </div>';
    $(wrapper).append(newElement);
  });

  $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
       e.preventDefault();
       $(this).parent('div').parent('div').remove();
   })

</script>
@stop
