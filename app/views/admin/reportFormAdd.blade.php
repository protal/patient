@extends('layouts.master')
@section('title')
  เพิ่มหัวข้อการบันทึก
@stop

@section('body')
  <div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">
      <h5>เพิ่มหัวข้อการบันทึก</h5>
    </div>

    <div class="panel-body">
      <form action="" method="post" data-parsley-validate class="form-horizontal form-label-left" novalidate>

        <fieldset class="form-group">
          <label for="exampleInputEmail1">หัวข้อการบันทึก</label>
          <input type="text" class="form-control" name="name" placeholder="หัวข้อการบันทึก"  required="required">
        </fieldset>

        <div class="text-right">
          <button type="submit" class="btn btn-primary">บันทึก</button>
        </div>

      </form>
    </div>
  </div>
@stop
