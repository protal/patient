@extends('layouts.master') @section('title') เพิ่มข้อมูลบ้านเลขที่ @stop

@section('body')
<div class="panel panel-default">
	<!-- Default panel contents -->
	<div class="panel-heading" style="height:55px;">
		<span
			style="font-size: 14px; vertical-align: -webkit-baseline-middle;">เพิ่มบ้านเลขที่</span>
	</div>

	<div class="panel-body">
		<form action="" method="post" data-parsley-validate
			class="form-horizontal form-label-left" novalidate>
			<input type="hidden" name="vid" value="{{$vid}}" />
			<input type="hidden" name="homeId" value="{{$homeId}}" />
			<div id="render">
				<fieldset class="form-group">
					<div class="form-group">
						<div class="col-md-3">
							<label for="subdistcode">บ้านเลขที่</label>
							<input type="text" name="hno" class="form-control" placeholder="บ้านเลขที่" value="{{$home->hno}}" required="required">
						</div>
						<div class="col-md-2">
							<label for="subdistcode">ละติจูด</label>
							<input type="text" name="latitude" class="form-control" placeholder="ละติจูด" value="{{$home->ygis}}">
						</div>
						<div class="col-md-2">
							<label for="subdistcode">ลองจิจูด</label>
							<input type="text" name="longtitude" class="form-control" placeholder="ลองจิจูด" value="{{$home->xgis}}">
						</div>
					</div>
				</fieldset>
			</div>

			<div class="text-right">
				<button type="submit" class="btn btn-primary">บันทึก</button>
			</div>
		</form>
	</div>
</div>

@stop 
