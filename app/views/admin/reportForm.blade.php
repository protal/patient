@extends('layouts.master')
@section('title')
  แก้ไขหัวข้อบันทึก
@stop

@section('body')
<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">
    <div class="btn-group pull-right">
        <a href="{{{url('admin/report/form/'.$form->id.'/edit')}}}" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> แก้ไข</a>
        <a href="{{{url('admin/report/form/'.$form->id.'/topic/add')}}}" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เพิ่ม</a>
    </div>
    <h5>{{$form->name}}</h5>
  </div>

  <!-- Table -->
  @if($topics->count()==0)
  <div class="panel-body text-center">
    ...ไม่พบหัวข้อในการบันทึก...
  </div>
  @else
  <table class="table ">
    <thead>
      <tr>
        <td>#</td>
        <td>หัวข้อ</td>
        <td>ตัวเลือก</td>
        <td width="20%">ดำเนินการ</td>
      </tr>
    </thead>
    <tbody>
      <?php $i = 0 ; ?>
      <?php foreach ($topics as $topic): ?>

        <tr>
          <td>{{++$i}}</td>
          <td>{{$topic->name}}</td>
          <td>
            <?php foreach ($topic->scores as $score): ?>
             <p><span class="badge badge-success">{{$score->score}} คะแนน</span> {{$score->name}} </p>
            <?php endforeach; ?>
          </td>
          <td>
            <a href="{{{url('admin/report/topic/'.$topic->id.'/edit')}}}" class="btn btn-primary  btn-sm"> <span class="glyphicon glyphicon-edit" aria-hidden="true"> แก้ไข</span></a>
            <a href="{{{url('admin/report/topic/'.$topic->id.'/delete')}}}" class="btn btn-danger  btn-sm delete"> <span class="glyphicon glyphicon-remove" aria-hidden="true"> ลบ</span></a>
          </td>
        </tr>
      <?php endforeach; ?>

    </tbody>
  </table>
  @endif
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    <div class="btn-group pull-right">
        <a href="{{{url('admin/report/form/'.$form->id.'/result/add')}}}" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เพิ่ม</a>
    </div>
    <h5>ผลประเมิน</h5>
  </div>
  @if($resultDescs->count()==0)
  <div class="panel-body text-center">
    ...ไม่พบหัวข้อในการบันทึก...
  </div>
  @else
  <table class="table ">
    <thead>
      <tr>
        <td>ผลประเมิน</td>
        <td>รายละเอียด</td>
        <td>ช่วงคะแนน</td>
        <td width="20%">ดำเนินการ</td>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($resultDescs as $resultDesc): ?>

        <tr>
          <td>{{$resultDesc->name}}</td>
          <td>{{$resultDesc->desc}}</td>
          <td>
            @if($resultDesc->score_end==0)
                ≥ {{$resultDesc->score_start }}
            @else
              {{$resultDesc->score_start }} -  {{$resultDesc->score_end }}
            @endif
          </td>
          <td>
            <a href="{{{url('admin/report/result/'.$resultDesc->id.'/edit')}}}" class="btn btn-primary  btn-sm"> <span class="glyphicon glyphicon-edit" aria-hidden="true"> แก้ไข</span></a>
            <a href="{{{url('admin/report/result/'.$resultDesc->id.'/delete')}}}" class="btn btn-danger  btn-sm delete"> <span class="glyphicon glyphicon-remove" aria-hidden="true"> ลบ</span></a>
          </td>
        </tr>
      <?php endforeach; ?>

    </tbody>
  </table>
  @endif
</div>
@stop
