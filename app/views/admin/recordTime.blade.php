@extends('layouts.master')
@section('title')
  จัดการครั้งที่บันทึก
@stop

@section('body')
<style media="screen">
  #Special {
  width: 200px;
  float: right;
  }

  .textColor{
    color: #000 !important ;
  }
</style>
<div class="panel panel-default">
  <div class="panel-heading">
  <h5 class="pull-left">ปีและครั้งที่บันทึก</h5>
    <form class="input-group" action="" method="get">
      <input id="Special" name="q" type="text" class="form-control" value="{{(isset($q)?$q:"")}}" placeholder="ค้นหาโดยปีที่ทำการบันทึกหรือครั้งที่ทำการบันทึก">
      <div class="input-group-btn">
        <button class="btn btn-primary"><i class="glyphicon glyphicon-search"></i></button>
        <a href="{{{url('admin/recordTime/add')}}}" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เพิ่ม</a>
      </div>
    </form>
  </div>

  @if ($recordTimes->count() > 0)
    <!-- Table -->
    <table class="table ">
      <thead>
        <tr>
          <td>ปีที่ทำการบันทึก</td>
          <td>ครั้งที่ทำการบันทึก</td>
          <td width="30%">ดำเนินการ</td>
        </tr>
      </thead>
      <tbody>
        <?php foreach($recordTimes as $recordTime): ?>
        <tr>
          <td>{{$recordTime->record_years}}</td>
          <td>{{$recordTime->record_times}}</td>
          <td>
              <a href="{{{url('admin/recordTime/edit/'.$recordTime->id)}}}" class="btn btn-primary  btn-sm textColor">
              <span class="glyphicon glyphicon-edit" aria-hidden="true"></span> แก้ไข</a>
              <a href="{{{url('admin/recordTime/delete/'.$recordTime->id)}}}" class="btn btn-danger  btn-sm delete textColor">
              <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> ลบ</a>
          </td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <div class="panel-body text-center">
        <?php echo $recordTimes->links(); ?>
    </div>
  @else
    <div class="panel-body text-center">
      ...ไม่พบข้อมูล...
    </div>
  @endif
</div>

@stop
