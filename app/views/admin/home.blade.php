@extends('layouts.master')
@section('title')
  จัดการบ้านเลขที่
@stop

@section('body')
<style media="screen">
  #Special {
  width: 200px;
  float: right;
  }

  .textColor{
    color: #000 !important ;
  }
</style>
<div class="panel panel-default">
  <div class="panel-heading">
  <h5 class="pull-left">รายละเอียดบ้านเลขที่</h5>
    <form class="input-group" action="" method="get">
      <input id="Special" name="q" type="text" class="form-control" value="{{(isset($q)?$q:"")}}" placeholder="ค้นหาโดยชื่อหรือหมู่ที่">
      <div class="input-group-btn">
        <button class="btn btn-primary"><i class="glyphicon glyphicon-search"></i></button>
        <a href="{{{url('admin/village/home/add/'.$vid)}}}" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เพิ่ม</a>
      </div>
    </form>
  </div>

  @if ($homes->count() > 0)
    <!-- Table -->
    <table class="table ">
      <thead>
        <tr>
          <td>ลำดับ</td>
          <td>บ้านเลขที่</td>
          <td width="30%">ดำเนินการ</td>
        </tr>
      </thead>
      <tbody>
        <?php foreach($homes as $index => $home): ?>
        <tr>
          <td>{{$index+1}}</td>
          <td>{{$home->hno}}</td>
          <td>
              <a href="{{{url('admin/village/home/edit/'.$vid.'/'.$home->id)}}}" class="btn btn-primary  btn-sm textColor">
              <span class="glyphicon glyphicon-edit" aria-hidden="true"></span> แก้ไข</a>
              <a href="{{{url('admin/village/home/delete/'.$vid.'/'.$home->id)}}}" class="btn btn-danger  btn-sm delete textColor">
              <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> ลบ</a>
          </td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <div class="panel-body text-center">
      <?php echo $homes->links(); ?>
    </div>
  @else
    <div class="panel-body text-center">
      ...ไม่พบข้อมูล...
    </div>
  @endif
</div>

@stop
