@extends('layouts.master')
@section('title')
  เพิ่มส่วนการประเมิน
@stop

@section('body')
  <div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">
      <h5>เพิ่มส่วนการประเมิน</h5>
    </div>

    <div class="panel-body">
      <form action="" method="post" data-parsley-validate class="form-horizontal form-label-left" novalidate>

        <fieldset class="form-group">
          <label for="exampleInputEmail1">หัวข้อการประเมิน</label>
          <input type="text" class="form-control" name="name" placeholder="หัวข้อ"  required="required" >
        </fieldset>

        <div class="text-right">
          <button type="submit" class="btn btn-primary">บันทึก</button>
        </div>

      </form>
    </div>
  </div>
@stop
