@extends('layouts.master') @section('title') เพิ่มข้อมูลบ้านเลขที่ @stop

@section('body')
<div class="panel panel-default">
	<!-- Default panel contents -->
	<div class="panel-heading" style="height:55px;">
		<span
			style="font-size: 14px; vertical-align: -webkit-baseline-middle;">เพิ่มบ้านเลขที่</span>
		<button type="button" id="addHno" class="btn btn-primary"
			style="float: right;">เพิ่มบ้านเลขที่</button>
	</div>

	<div class="panel-body">
		<form action="" method="post" data-parsley-validate
			class="form-horizontal form-label-left" novalidate>
			<input type="hidden" name="vid" value="{{$vid}}" />
			<div id="render"></div>

			<div class="text-right">
				<button type="submit" class="btn btn-primary">บันทึก</button>
			</div>
		</form>
	</div>
</div>

@stop @section('js')
<script type="text/javascript">

	var index = 0;
	var add = $("#addHno");
	var tempData = [{
		hno: "",
		latitude: "",
		longtitude: ""
	}];

	init();
	
	add.click(function(){
		if(index < 5){
			var data = {
				hno: "",
				latitude: "",
				longtitude: ""
			};
			tempData.push(data);
			render();
		}
	});

	function init(){
		render();
	}

	function remove(id){
		tempData = jQuery.grep(tempData, function( n, i ) {
            return i != id;
        });

        $("#form-"+id).remove();
        index--;
    }

	function render(){
		var htmlRender = "";
// 		$.each(tempData, function( n, value ) {
		htmlRender += '<fieldset class="form-group" id="form-'+index+'">'
          +'<div class="form-group">'
            +'<div class="col-md-3">'
              +'<label for="subdistcode">บ้านเลขที่</label>'
              +'<input type="text" name="hno[]" class="form-control"  placeholder="บ้านเลขที่"  value="" required="required">'
            +'</div>'
            +'<div class="col-md-2">'
              +'<label for="subdistcode">ละติจูด</label>'
              +'<input type="text" name="latitude[]" class="form-control"  placeholder="ละติจูด"  value="">'
            +'</div>'
            +'<div class="col-md-2">'
              +'<label for="subdistcode">ลองจิจูด</label>'
              +'<input type="text" name="longtitude[]" class="form-control"  placeholder="ลองจิจูด"  value="">'
            +'</div>';

            if(index != 0){
 	            htmlRender += '<div class="col-md-1">'
 	              +'<label for="subdistcode"></label>'
 	              +'<button type="button" class="form-control btn btn-danger" style="float:right;" onclick="remove('+index+')">ลบ</button>'
 	            +'</div>';
            }

            
           htmlRender += '</div>'
        	+'</fieldset>';

           $("#render").append(htmlRender);
// 		});

// 		render.html(htmlRender);
		index++;
	}


</script>
@stop
