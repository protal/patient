@extends('layouts.master')
@section('title')
  จัดการบันทึก
@stop

@section('body')
<div class="text-right">
  <a href="{{{url('admin/report/part/add')}}}" class="btn btn-default"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เพิ่มส่วนของการประเมิน</a>
  <br>
  <br>
</div>
<?php $i = 0 ; ?>
@foreach ($parts as $part)
  <div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">
        <div class="btn-group pull-right">
          <a href="{{{url('admin/report/part/'.$part->id)}}}" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-edit" aria-hidden="true"> แก้ไข</a>
          <a href="{{{url('admin/report/part/'.$part->id.'/form/add')}}}" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เพิ่ม</a>
          <a href="{{{url('admin/report/part/'.$part->id.'/delete')}}}" class="btn btn-danger btn-sm delete"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> ลบ</a>
        </div>
        <h5>
          ส่วนที่ {{++$i}} {{$part->name}}
        </h5>
    </div>
    @if($part->forms->count()==0)
    <div class="panel-body text-center">
      ...ไม่พบหัวข้อในการบันทึก...
    </div>

    @else
      <!-- Table -->
      <table class="table table-striped">
        <thead>
          <tr>
            <td width="75%">หัวข้อบันทึก</td>
            <td>ดำเนินการ</td>
          </tr>
        </thead>
        <tbody>
          @foreach ($part->forms as $form)
          <tr>
            <td>{{$form->name}}</td>
            <td>
              <a href="{{{url('admin/report/form/'.$form->id)}}}" class="btn btn-primary  btn-sm"> <span class="glyphicon glyphicon-edit" aria-hidden="true"> แก้ไข</span></a>
              <a href="{{{url('admin/report/form/'.$form->id.'/delete')}}}" class="btn btn-danger  btn-sm delete"> <span class="glyphicon glyphicon-remove" aria-hidden="true"> ลบ</span></a>
            </td>
          </tr>
          @endforeach
        </tdbody>
      </table>
    @endif


  </div>
@endforeach


@stop
