@extends('layouts.master')
@section('title')
  แก้ไขหัวข้อ
@stop

@section('body')
  <div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">
      <h5>แก้ไขหัวข้อ</h5>
    </div>

    <div class="panel-body">
      <form action="" method="post" data-parsley-validate novalidate>

        <fieldset class="form-group">
          <label for="exampleInputEmail1">ชื่อหัวข้อ</label>
          <input type="text" class="form-control" name="name" required="required" value="{{$topic->name}}" placeholder="หัวข้อบันทึก">
        </fieldset>
        <hr>

        <a class="add_field_button btn btn-info">เพิ่มคะแนน</a>

        <fieldset>
          <div class="wrap-input">
            <?php $i = 0;  ?>
            @foreach ($topic->scores as $score)
              @if($i++==0)
              <div class="row form-add">
                    <input type="hidden" name="scoreid[]" value="{{$score->id}}">
                <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                    <label for="exampleInputEmail1">ชื่อคะแนน</label>
                    <input type="text" name="scorename[]" placeholder="ชื่อคะแนน" required="required" class="form-control" value="{{$score->name}}">
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                    <label for="exampleInputEmail1">หน่วยคะแนน</label>
                    <input type="number" name="score[]" placeholder="หน่วยคะแนน" required="required" class="form-control" value="{{$score->score}}">
                </div>
              </div>
              @else
              <div class="row form-add">
                <input type="hidden" name="scoreid[]" value="{{$score->id}}">
                <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                  <label for="exampleInputEmail1">ชื่อคะแนน</label>
                  <input type="text" name="scorename[]" placeholder="ชื่อคะแนน" class="form-control" required="required" value="{{$score->name}}">
                </div>
                <div class="col-md-5 col-sm-5 col-xs-5 form-group">
                  <label for="exampleInputEmail1">หน่วยคะแนน</label>
                  <input type="number" name="score[]" placeholder="หน่วยคะแนน" class="form-control" required="required" value="{{$score->score}}">
                </div>
                <div class="col-md-1 col-sm-1 col-xs-1 form-group">
                  <br>
                  <button href="#" class="btn btn-danger btn-sm remove_field" name="{{$score->id}}">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"> ลบ</span>
                  </button>
                </div>
              </div>
              @endif
            @endforeach

            <!-- <div class="row form-add">
              <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                <label for="exampleInputEmail1">ชื่อคะแนน</label>
                <input type="text" name="scorename[]" placeholder="ชื่อคะแนน" class="form-control" required="required">
              </div>
              <div class="col-md-5 col-sm-5 col-xs-5 form-group">
                <label for="exampleInputEmail1">หน่วยคะแนน</label>
                <input type="number" name="score[]" placeholder="หน่วยคะแนน" class="form-control" required="required">
              </div>
              <div class="col-md-1 col-sm-1 col-xs-1 form-group">
                <br>
                <button href="#" class="btn btn-danger btn-sm remove_field">
                  <span class="glyphicon glyphicon-remove" aria-hidden="true"> ลบ</span>
                </button>
              </div>
            </div> -->

          </div>


        </fieldset>
        <div class="text-right">
          <button type="submit"  class="btn btn-primary">บันทึก</button>
        </div>

      </form>
    </div>
  </div>
@stop

@section('js')

<script type="text/javascript">
  var wrapper         = $(".wrap-input");
  var add_button      = $(".add_field_button");
  var moredata      = $(".more-data");

  $(add_button).click(function(e){
    e.preventDefault();
    var newElement='<div class="row form-add"><input type="hidden" name="scoreid[]" value="-1"> <div class="col-md-6 col-sm-6 col-xs-6 form-group"> <label for="exampleInputEmail1">ชื่อคะแนน</label> <input type="text" name="scorename[]" placeholder="ชื่อคะแนน" class="form-control" required="required"> </div> <div class="col-md-5 col-sm-5 col-xs-5 form-group"> <label for="exampleInputEmail1">หน่วยคะแนน</label> <input type="number" name="score[]" placeholder="หน่วยคะแนน" class="form-control" required="required"> </div> <div class="col-md-1 col-sm-1 col-xs-1 form-group"> <br> <button href="#" class="btn btn-danger btn-sm remove_field"> <span class="glyphicon glyphicon-remove" aria-hidden="true"> ลบ</span></button> </div> </div>';
    $(wrapper).append(newElement);
  });

  $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
       e.preventDefault();
       $(this).parent('div').parent('div').remove();

       var newDelete = '<input type="hidden" name="del[]" value="'+$(this).prop('name')+'">';
       $(wrapper).append(newDelete);
       console.log($(this).prop('name'))
   })

</script>
@stop
