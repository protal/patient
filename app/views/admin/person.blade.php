@extends('layouts.master')
@section('title')
  จัดการอาสาสมัครหมู่บ้าน
@stop

@section('body')
<style media="screen">
  #Special {
  width: 200px;
  float: right;
  }

  .textColor{
    color: #000 !important ;
  }
</style>
<div class="panel panel-default">
  <div class="panel-heading">
  <h5 class="pull-left">รายชื่ออาสาสมัครหมู่บ้าน</h5>
    <form class="input-group" action="" method="get">
      <input id="Special" name="q" type="text" class="form-control" value="{{(isset($q)?$q:"")}}" placeholder="ค้นหาโดยชื่อหรือนามสกุล">
      <div class="input-group-btn">
        <button class="btn btn-primary"><i class="glyphicon glyphicon-search"></i></button>
        <a href="{{{url('admin/person/add')}}}" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เพิ่ม</a>
      </div>
    </form>
  </div>

@if ($persons->count() > 0)
  <!-- Table -->
  <table class="table ">
    <thead>
      <tr>
        <td>ชื่อ</td>
        <td>นามสกุล</td>
        <td width="30%">ดำเนินการ</td>
      </tr>
    </thead>
    <tbody>
      <?php foreach($persons as $person): ?>
      <tr>
        <td>{{$person->fname}}</td>
        <td>{{$person->lname}}</td>
        <td>
            <a href="{{{url('admin/person/edit/'.$person->pid)}}}" class="btn btn-primary  btn-sm textColor">
            <span class="glyphicon glyphicon-edit" aria-hidden="true"></span> แก้ไข</a>
            <a href="{{{url('admin/person/delete/'.$person->pid)}}}" class="btn btn-danger  btn-sm delete textColor">
            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> ลบ</a>
        </td>
      </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
  <div class="panel-body text-center">
    <?php echo $persons->links(); ?>
  </div>
@else
  <div class="panel-body text-center">
    ...ไม่พบข้อมูล...
  </div>
@endif
</div>
@stop
