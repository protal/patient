@extends('layouts.master')
@section('title')
  แก้ไขข้อมูลหมู่บ้าน
@stop

@section('body')
  <div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">
      <h5>แก้ไขชื่อตำบลและหมู่ที่ต้องการบันทึก</h5>
    </div>

    <div class="panel-body">
      <form action="" method="post" data-parsley-validate class="form-horizontal form-label-left" novalidate>

        <fieldset class="form-group">
          <div class="row">
            <div class="col-md-6">
              <label for="subdistcode">ตำบลที่ทำการบันทึก</label>
              <input type="text" class="form-control"  placeholder="ตำบลที่ทำการบันทึก"  required="required" value="ไทยบุรี" disabled>
            </div>
            <div class="col-md-6">
              <label for="village">หมู่ที่ทำการบันทึก</label>
              <input type="text" class="form-control" name="village" placeholder="หมู่ที่ทำการบันทึก"  required="required" value="{{$village->village}}">
            </div>
            <div class="col-md-6">
              <label for="centercoord">ศูนย์กลางพิกัด</label>
              <input type="text" class="form-control" name="centercoord" placeholder="ศูนย์กลางพิกัด"  required="required" value="{{$village->centercoord}}">
            </div>
            <div class="col-md-6">
              <label for="centercoord">เส้นขอบพิกัด</label>
              <input type="text" class="form-control" name="edgecoord" placeholder="เส้นขอบพิกัด" value="{{$village->edgecoord}}">
            </div>
            <div class="col-md-6">
              <label for="color">สีพื้นที่หมู่บ้าน</label>
              <select  class="form-control" name="color" required >
                <option value="">กรุณาเลือกสีหมุดแผนที่</option>
                <option value="blue" {{$village->blue}}>สีน้ำเงิน</option>
                <option value="orange" {{$village->orange}}>สีส้ม</option>
                <option value="purple" {{$village->purple}}>สีม่วง</option>
                <option value="green" {{$village->green}}>สีเขียว</option>
                <option value="pink" {{$village->pink}}>สีชมพู</option>
                <option value="yellow" {{$village->yellow}}>สีเหลือง</option>
              </select>
            </div>
          </div>
        </fieldset>

        <div class="text-right">
          <button type="submit" class="btn btn-primary">บันทึก</button>
        </div>

      </form>
    </div>
  </div>

@stop
