@extends('layouts.master')
@section('title')
  เพิ่มประวัติส่วนตัวอาสาสมัครหมู่บ้าน
@stop
<?php
  $disabled = "disabled" ;
 ?>
@section('css')

  <link href="{{ URL::asset('theme/tagsInput/src/jquery.tagsinput.css'); }}" rel="stylesheet">
  <style media="screen">
    div.tagsinput span.tag{
      background: #0e52a2;
      border: 1px solid #000610;
    }
  </style>
@stop
@section('body')
    <div class="container">
      <div class="row">
        <form class="" action="" method="post" data-parsley-validate novalidate>
        <!-- panel 1 -->
        <div class="col-md-12">
          <div class="panel panel-default">
            <!-- Default panel1 contents -->
            <div class="panel-heading">
              <h5>เพิ่มประวัติส่วนตัวอาสาสมัครหมู่บ้าน</h5>
            </div>
            <!-- Table1 -->
            <div class="panel-body">
              <!-- 2 form -->

              <div class="row">
                <div class="col-md-2 control-label">
                  <label for="name" style="padding-top:5px;">ชื่อจริง</label>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <input type="text" class="form-control" name="fname" placeholder="ชื่อจริง" required>
                  </div>
                </div>
                <div class="col-md-2 control-label">
                  <label for="lastname" style="padding-top:5px;">นามสกุล</label>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <input type="text" class="form-control" name="lname" placeholder="นามสกุล" required>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-2 control-label">
                  <label for="villcode" style="padding-top:5px;">รับผิดชอบหมู่ที่</label>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <select  class="form-control" name="villcode" required >
                      @foreach($villcode as $v)
                        <option value="{{$v->village}}" >{{$v->village}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-2 control-label">
                  <label for="name" style="padding-top:5px;">รับผิดชอบบ้านเลขที่</label>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <input type="text" class="form-control" name="hno" id="tags_1" placeholder="รับผิดชอบบ้านเลขที่" required>
                  </div>
                </div>
              </div>
              
              <hr/>
              
              <div id="homeNumber"></div>

              <div class="text-right">
                <button type="submit" class="btn btn-primary">บันทึก</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

@stop

@section('js')
<script src="{{ URL::asset('theme/dist/jquery.inputmask.bundle.js'); }}"></script>
<script src="{{ URL::asset('theme/tagsInput/src/jquery.tagsinput.js'); }}"></script>
<script type="text/javascript">]

  $( ".prefix" )
  .change(function() {
    var val = $(this).find("option:selected").val();
    if (val==="นาย") {
      $(".gender").val("1");
    }
    else if (val==="นาง" || val==="นางสาว") {
      $(".gender").val("0");
    }
    else {
      $(".gender").val("");
    }
  })
  .trigger( "change" );
</script>
@stop
