@extends('layouts.master')
@section('title')
  เพิ่มผลประเมิน
@stop

@section('css')
  <style media="screen">
    .selcolor{
      -webkit-appearance: none;
    }
  </style>
@stop

@section('body')
  <div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">
      <h5>เพิ่มผลประเมิน</h5>
    </div>

    <div class="panel-body">
      <form action="" method="post" data-parsley-validate novalidate>

        <fieldset class="form-group">
          <label for="exampleInputEmail1">ผลประเมิน</label>
          <input type="text" class="form-control" name="name" value="{{$result->name}}" placeholder="ผลประเมิน .. ผู้สูงอายุกลุ่มติดสังคม" required>
        </fieldset>

        <fieldset class="form-group">
          <label for="">ราละเอียด</label>
          <textarea name="desc" class="form-control" rows="3" cols="80" placeholder="คำอธิบาย" >{{$result->desc}}</textarea>
        </fieldset>

        <fieldset class="form-group">
          <label for="">ช่วงคะแนน</label>
          <div class="row">
            <div class="col-md-6">
              <input type="number" class="form-control" name="score_start"  value="{{$result->score_start}}"  placeholder="เริ่มต้น" required>
            </div>
            <div class="col-md-6">
              <input type="number" class="form-control" name="score_end" value="{{$result->score_end}}" placeholder="สิ้นสุด , 0 หมายถึงไม่สิ้นสุด" required>
            </div>
          </div>
        </fieldset>

        <fieldset class="form-group">

          <div class="row">
            <div class="col-md-6">
              <label for="">สีหมุดของผลลัพธ์</label>
              <select class="form-control selcolor" name="pincolor">
                <option value="1" style="background: rgba(0, 255, 20, 0.24); color: #000;" {{($result->pincolor==1)?"selected":""}}>สีเขียว</option>
                <option value="2" style="background: rgba(255, 0, 0, 0.24); color: #000;" {{($result->pincolor==2)?"selected":""}}>สีแดง</option>
                <option value="3" style="background: rgba(255, 129, 0, 0.24); color: #00;" {{($result->pincolor==3)?"selected":""}}>สีส้ม</option>
              </select>
            </div>
            <div class="col-md-6">
              <label for="">สีแถบของผลลัพธ์</label>
              <select class="form-control selcolor" name="rowcolor">
                <option value="1" style="background: rgba(0, 255, 20, 0.24); color: #000;" {{($result->rowcolor==1)?"selected":""}}>สีเขียว</option>
                <option value="2" style="background: rgba(255, 212, 0, 0.24); color: #000;" {{($result->rowcolor==2)?"selected":""}}>สีเหลือง</option>
                <option value="3" style="background: rgba(255, 0, 0, 0.24); color: #000;" {{($result->rowcolor==3)?"selected":""}}>สีแดง</option>
                <option value="4" style="background: rgba(255, 71, 0, 0.24); color: #000;" {{($result->rowcolor==4)?"selected":""}}>สีแดงอ่อน</option>
                <option value="5" style="background: rgba(255, 129, 0, 0.24); color: #00;" {{($result->rowcolor==5)?"selected":""}}>สีส้ม</option>
              </select>
            </div>
          </div>
        </fieldset>


        <div class="text-right">
          <button type="submit"  class="btn btn-primary">บันทึก</button>
        </div>

      </form>
    </div>
  </div>
@stop

@section('js')

@stop
