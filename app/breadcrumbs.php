<?php
  Breadcrumbs::register('home', function($breadcrumbs) {
    $breadcrumbs->push('หน้าแรก', route('home'));
  });



  Breadcrumbs::register('patient', function($breadcrumbs) {
    $breadcrumbs->push('บันทึกสุขภาพ', route('patient'));
  });

  Breadcrumbs::register('patient.add', function($breadcrumbs) {
    $breadcrumbs->parent('patient');
    $breadcrumbs->push('เพิ่มประวัติส่วนตัวผู้สูงอายุ', route('patient.add'));
  });

  Breadcrumbs::register('patient.edit', function($breadcrumbs) {
    $breadcrumbs->parent('patient');
    $breadcrumbs->push('แก้ไขประวัติส่วนตัวผู้สูงอายุ', route('patient.edit'));
  });

  Breadcrumbs::register('patient.report', function($breadcrumbs , $patient_id) {
    $patient = Patient::find($patient_id);
    $breadcrumbs->parent('patient');
    $breadcrumbs->push($patient->prefix.' '.$patient->Firstname.' '.$patient->Sirname, route('patient.report', [$patient->id] ));
  });
  Breadcrumbs::register('patient.record', function($breadcrumbs , $patient_id , $form_id) {
    $form = Forms::find($form_id);
    $breadcrumbs->parent('patient.report',$patient_id);
    $breadcrumbs->push($form->name, route('patient.record', [$patient_id,$form_id]));
  });

  Breadcrumbs::register('patient.record.add', function($breadcrumbs , $patient_id , $form_id) {
    $breadcrumbs->parent('patient.record', $patient_id , $form_id);
    $breadcrumbs->push('ลงบันทึก', route('patient.record.add',[$patient_id,$form_id]));
  });
  Breadcrumbs::register('patient.record.edit', function($breadcrumbs , $patient_id , $form_id) {
    $breadcrumbs->parent('patient.record', $patient_id , $form_id);
    $breadcrumbs->push('แก้ไขบันทึก', route('patient.record.edit',[$patient_id,$form_id]));
  });

  Breadcrumbs::register('graph', function($breadcrumbs) {
    $breadcrumbs->push('กราฟสรุปบันทึกสุขภาพ', route('graph'));
  });
  Breadcrumbs::register('graph.result', function($breadcrumbs , $evoluation_part, $form_id) {
    $form = Forms::find($form_id);
    $breadcrumbs->parent('graph' , $evoluation_part ,$form_id );
    $breadcrumbs->push('สรุปบันทึกสุขภาพหัวข้อ'.$form->name, route('graph.result' , [$evoluation_part,$form_id]));
  });

  Breadcrumbs::register('conclude', function($breadcrumbs) {
      $breadcrumbs->push('สรุปสุขภาพผู้สูงอายุ', route('conclude'));
  });

  Breadcrumbs::register('conclude.result', function($breadcrumbs , $evoluation_part, $form_id) {
      $form = Forms::find($form_id);
      $breadcrumbs->parent('conclude' , $evoluation_part ,$form_id );
      $breadcrumbs->push('สรุปสุขภาพผู้สูงอายุหัวข้อ'.$form->name, route('conclude.result' , [$evoluation_part,$form_id]));
  });

  Breadcrumbs::register('admin.report', function($breadcrumbs) {
    $breadcrumbs->push('จัดการบันทึก', route('admin.report'));
  });
  Breadcrumbs::register('admin.report.part.add', function($breadcrumbs) {
    $breadcrumbs->parent('admin.report');
    $breadcrumbs->push('เพิ่มส่วนการประเมิน', route('admin.report.part.add'));
  });
  Breadcrumbs::register('admin.report.part.edit', function($breadcrumbs) {
    $breadcrumbs->parent('admin.report');
    $breadcrumbs->push('แก้ไขส่วน', route('admin.report.part.edit'));
  });
  Breadcrumbs::register('admin.report.part.form.add', function($breadcrumbs) {
    $breadcrumbs->parent('admin.report');
    $breadcrumbs->push('เพิ่มหัวข้อการบันทึก', route('admin.report.part.form.add'));
  });
  Breadcrumbs::register('admin.report.form', function($breadcrumbs , $form_id) {
    $form = Forms::find($form_id);
    $breadcrumbs->parent('admin.report' , $form_id);
    $breadcrumbs->push('แก้ไขหัวข้อ'.$form->name, route('admin.report.form' , [$form_id]));
  });
  Breadcrumbs::register('admin.report.form.edit', function($breadcrumbs , $form_id) {
    $breadcrumbs->parent('admin.report.form' , $form_id);
    $breadcrumbs->push('แก้ไขหัวข้อ', route('admin.report.form.edit' , [$form_id]));
  });
  Breadcrumbs::register('admin.report.form.topic.add', function($breadcrumbs , $form_id) {
    $breadcrumbs->parent('admin.report.form' , $form_id);
    $breadcrumbs->push('เพิ่มหัวข้อ', route('admin.report.form.topic.add' , [$form_id]));
  });
  Breadcrumbs::register('admin.report.topic.edit', function($breadcrumbs , $topic_id) {
    $topic = Topic::find($topic_id);
    $breadcrumbs->parent('admin.report.form' , $topic->evoluation_form_form_id);
    $breadcrumbs->push('แก้ไขหัวข้อ', route('admin.report.topic.edit' , $topic_id));
  });
  Breadcrumbs::register('admin.report.result.edit', function($breadcrumbs , $resultDescs_id) {
    $resultDescs = ResultDesc::find($resultDescs_id);
    $breadcrumbs->parent('admin.report.form' , $resultDescs->form_id);
    $breadcrumbs->push('แก้ไขเพิ่มผลประเมิน', route('admin.report.result.edit' , $resultDescs_id));
  });
  Breadcrumbs::register('admin.report.form.result.add', function($breadcrumbs , $form_id) {
    $breadcrumbs->parent('admin.report.form' , $form_id);
    $breadcrumbs->push('เพิ่มผลประเมิน', route('admin.report.form.result.add' , $form_id));
  });

  Breadcrumbs::register('admin.user', function($breadcrumbs) {
    $breadcrumbs->push('จัดการสมาชิก', route('admin.user'));
  });
  Breadcrumbs::register('admin.user.add', function($breadcrumbs) {
    $breadcrumbs->parent('admin.user');
    $breadcrumbs->push('เพิ่มสมาชิก', route('admin.user.add'));
  });
  Breadcrumbs::register('admin.user.edit', function($breadcrumbs) {
    $breadcrumbs->parent('admin.user');
    $breadcrumbs->push('แก้ไขสมาชิก', route('admin.user.edit'));
  });

  Breadcrumbs::register('admin.recordTime', function($breadcrumbs) {
    $breadcrumbs->push('จัดการครั้งที่บันทึก', route('admin.recordTime'));
  });
  Breadcrumbs::register('admin.recordTime.add', function($breadcrumbs) {
    $breadcrumbs->parent('admin.recordTime');
    $breadcrumbs->push('เพิ่มปีและครั้งที่ทำการบันทึก', route('admin.recordTime.add'));
  });
  Breadcrumbs::register('admin.recordTime.edit', function($breadcrumbs) {
    $breadcrumbs->parent('admin.recordTime');
    $breadcrumbs->push('แก้ไขปีและครั้งที่ทำการบันทึก', route('admin.recordTime.edit'));
  });

  Breadcrumbs::register('admin.village', function($breadcrumbs) {
    $breadcrumbs->push('จัดการข้อมูลหมู่บ้าน', route('admin.village'));
  });
  Breadcrumbs::register('admin.village.add', function($breadcrumbs) {
    $breadcrumbs->parent('admin.village');
    $breadcrumbs->push('เพิ่มตำบลและหมู่ที่ต้องการบันทึก', route('admin.village.add'));
  });
  Breadcrumbs::register('admin.village.edit', function($breadcrumbs) {
    $breadcrumbs->parent('admin.village');
    $breadcrumbs->push('แก้ไขตำบลและหมู่ที่ต้องการบันทึก', route('admin.village.edit'));
  });
  
  Breadcrumbs::register('admin.village.home', function($breadcrumbs) {
      $breadcrumbs->parent('admin.village');
      $breadcrumbs->push('จัดการบ้านเลขที่', route('admin.village.home'));
  });
  
      Breadcrumbs::register('admin.village.home.add', function($breadcrumbs, $vid) {
      $breadcrumbs->parent('admin.village');
      $breadcrumbs->push('จัดการบ้านเลขที่', route('admin.village.home', $vid));
      $breadcrumbs->push('เพิ่มข้อมูลบ้านเลขที่', route('admin.village.home.add'));
  });

  Breadcrumbs::register('admin.person', function($breadcrumbs) {
    $breadcrumbs->push('จัดการข้อมูลอาสาสมัครหมู่บ้าน', route('admin.person'));
  });
  Breadcrumbs::register('admin.person.add', function($breadcrumbs) {
    $breadcrumbs->parent('admin.person');
    $breadcrumbs->push('เพิ่มข้อมูลข้อมูลอาสาสมัครหมู่บ้าน', route('admin.person.add'));
  });
  Breadcrumbs::register('admin.person.edit', function($breadcrumbs) {
    $breadcrumbs->parent('admin.person');
    $breadcrumbs->push('แก้ไขข้อมูลอาสาสมัครหมู่บ้าน.', route('admin.person.edit'));
  });
